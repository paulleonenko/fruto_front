import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

import { isPortrait, isMobile, getOffset } from "../utils/device";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/faq",
    name: "Faq",
    component: () => import("../views/Faq.vue"),
  },
  {
    path: "/account/profile",
    name: "Account",
    component: () => import("../views/account/Profile.vue"),
  },
  {
    path: "/account/history",
    name: "Account",
    component: () => import("../views/account/History.vue"),
  },
  {
    path: "/account/prizes",
    name: "Account",
    component: () => import("../views/account/Prizes.vue"),
  },
  {
    path: "/winners",
    name: "Winners",
    component: () => import("../views/Winners.vue"),
  },
  {
    path: "/launch",
    name: "Launch",
    component: () => import("../views/Launch.vue"),
  },
  {
    path: "/cards",
    name: "Cards",
    component: () => import("../views/Cards.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior: (to) => {
    let burger = document.querySelector(".header__menu_mob");
    if (burger) burger.classList.remove("active");

    let burger2 = document.querySelector(".header--mob_active");
    if (burger2) burger2.classList.remove("header--mob_active");

    if (to.hash) {
      let elem = document.querySelector(to.hash);

      let y = elem.offsetTop + 50;

      return { x: 0, y: y };
    }

    return { x: 0, y: 0 };
  },
});

export default router;
