(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.i00012 = function() {
	this.initialize(img.i00012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,538,150);


(lib.i00015 = function() {
	this.initialize(img.i00015);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,145,48);


(lib.i00017 = function() {
	this.initialize(img.i00017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,220,48);


(lib.i00020 = function() {
	this.initialize(img.i00020);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,100,86);


(lib.i00022 = function() {
	this.initialize(img.i00022);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00023 = function() {
	this.initialize(img.i00023);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00024 = function() {
	this.initialize(img.i00024);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00025 = function() {
	this.initialize(img.i00025);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00026 = function() {
	this.initialize(img.i00026);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00027 = function() {
	this.initialize(img.i00027);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00028 = function() {
	this.initialize(img.i00028);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00029 = function() {
	this.initialize(img.i00029);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00030 = function() {
	this.initialize(img.i00030);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00031 = function() {
	this.initialize(img.i00031);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00032 = function() {
	this.initialize(img.i00032);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00033 = function() {
	this.initialize(img.i00033);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00034 = function() {
	this.initialize(img.i00034);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00035 = function() {
	this.initialize(img.i00035);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00036 = function() {
	this.initialize(img.i00036);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00037 = function() {
	this.initialize(img.i00037);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00038 = function() {
	this.initialize(img.i00038);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00039 = function() {
	this.initialize(img.i00039);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00040 = function() {
	this.initialize(img.i00040);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00041 = function() {
	this.initialize(img.i00041);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00042 = function() {
	this.initialize(img.i00042);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00043 = function() {
	this.initialize(img.i00043);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00044 = function() {
	this.initialize(img.i00044);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.i00045 = function() {
	this.initialize(img.i00045);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,710,300);


(lib.i00046 = function() {
	this.initialize(img.i00046);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,718,300);


(lib.i00047 = function() {
	this.initialize(img.i00047);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,717,300);


(lib.i00048 = function() {
	this.initialize(img.i00048);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,720,300);


(lib.i00049 = function() {
	this.initialize(img.i00049);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,559,300);


(lib.i0005 = function() {
	this.initialize(img.i0005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,538,398);


(lib.i00050 = function() {
	this.initialize(img.i00050);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,32);


(lib.i00051 = function() {
	this.initialize(img.i00051);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,32);


(lib.i00052pngcopy = function() {
	this.initialize(img.i00052pngcopy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,58);


(lib.i00052pngcopy2 = function() {
	this.initialize(img.i00052pngcopy2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,58);


(lib.i00053pngcopy = function() {
	this.initialize(img.i00053pngcopy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,58);


(lib.i00053pngcopy2 = function() {
	this.initialize(img.i00053pngcopy2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,58);


(lib.i00054 = function() {
	this.initialize(img.i00054);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,84,84);


(lib.i00055 = function() {
	this.initialize(img.i00055);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,84,84);


(lib.i00056 = function() {
	this.initialize(img.i00056);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,84,84);


(lib.i00058 = function() {
	this.initialize(img.i00058);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,460,555);


(lib.i00059 = function() {
	this.initialize(img.i00059);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,48,48);


(lib.i0009 = function() {
	this.initialize(img.i0009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,412,131);


(lib.logof2 = function() {
	this.initialize(img.logof2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,122,118);


(lib.txt2x2 = function() {
	this.initialize(img.txt2x2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,514,36);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00059();
	this.instance.parent = this;
	this.instance.setTransform(-12,-12,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol32, new cjs.Rectangle(-12,-12,24,24), null);


(lib.Symbol30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AnzH0IAAvnIPnAAIAAPng");
	this.shape.setTransform(50,50);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol30, new cjs.Rectangle(0,0,100,100), null);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00056();
	this.instance.parent = this;
	this.instance.setTransform(-21,-21,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol29, new cjs.Rectangle(-21,-21,42,42), null);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00055();
	this.instance.parent = this;
	this.instance.setTransform(-21,-21,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol28, new cjs.Rectangle(-21,-21,42,42), null);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00054();
	this.instance.parent = this;
	this.instance.setTransform(-21,-21,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol27, new cjs.Rectangle(-21,-21,42,42), null);


(lib.Symbol26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00058();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol26, new cjs.Rectangle(0,0,460,555), null);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5D4343").s().p("EgmRAkaMAAAhIzMBMjAAAMAAABIzg");
	this.shape.setTransform(245,233);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(0,0,490,466), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Eg4PAu4MAAAhdvMBwfAAAMAAABdvg");
	this.shape.setTransform(250,300,0.694,1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(0,0,500,600), null);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("EgguARMMAAAgiXMBBdAAAMAAAAiXg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(-209.5,-110,419,220), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AhzDIIAAmPIDnAAIAAGPg");
	this.shape.setTransform(11.6,6,1.823,1.375);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(-9.4,-21.5,42.1,55), null);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00050();
	this.instance.parent = this;
	this.instance.setTransform(-10,-8,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(-10,-8,20,16), null);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("Aj5D6IAAnzIHzAAIAAHzg");
	this.shape.setTransform(25,25);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(0,0,50,50), null);


(lib.Symbol15copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AkIFKIAAqTIIRAAIAAKTg");
	this.shape.setTransform(0,0,1.469,1,0,0,0,-26.5,-33);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15copy, new cjs.Rectangle(0,0,77.9,66), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AkIFKIAAqTIIRAAIAAKTg");
	this.shape.setTransform(26.5,33);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0,0,53,66), null);


(lib.frame = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00020();
	this.instance.parent = this;
	this.instance.setTransform(-50,-43);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.frame, new cjs.Rectangle(-50,-43,100,86), null);


(lib.pics = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(23));

	// Layer 1
	this.instance = new lib.i00022();
	this.instance.parent = this;
	this.instance.setTransform(-150,-150);

	this.instance_1 = new lib.i00023();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-150,-150);

	this.instance_2 = new lib.i00024();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-150,-150);

	this.instance_3 = new lib.i00025();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-150,-150);

	this.instance_4 = new lib.i00026();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-150,-150);

	this.instance_5 = new lib.i00027();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-150,-150);

	this.instance_6 = new lib.i00028();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-150,-150);

	this.instance_7 = new lib.i00029();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-150,-150);

	this.instance_8 = new lib.i00030();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-150,-150);

	this.instance_9 = new lib.i00031();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-150,-150);

	this.instance_10 = new lib.i00032();
	this.instance_10.parent = this;
	this.instance_10.setTransform(-150,-150);

	this.instance_11 = new lib.i00033();
	this.instance_11.parent = this;
	this.instance_11.setTransform(-150,-150);

	this.instance_12 = new lib.i00034();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-150,-150);

	this.instance_13 = new lib.i00035();
	this.instance_13.parent = this;
	this.instance_13.setTransform(-150,-150);

	this.instance_14 = new lib.i00036();
	this.instance_14.parent = this;
	this.instance_14.setTransform(-150,-150);

	this.instance_15 = new lib.i00037();
	this.instance_15.parent = this;
	this.instance_15.setTransform(-150,-150);

	this.instance_16 = new lib.i00038();
	this.instance_16.parent = this;
	this.instance_16.setTransform(-150,-150);

	this.instance_17 = new lib.i00039();
	this.instance_17.parent = this;
	this.instance_17.setTransform(-150,-150);

	this.instance_18 = new lib.i00040();
	this.instance_18.parent = this;
	this.instance_18.setTransform(-150,-150);

	this.instance_19 = new lib.i00041();
	this.instance_19.parent = this;
	this.instance_19.setTransform(-150,-150);

	this.instance_20 = new lib.i00042();
	this.instance_20.parent = this;
	this.instance_20.setTransform(-150,-150);

	this.instance_21 = new lib.i00043();
	this.instance_21.parent = this;
	this.instance_21.setTransform(-150,-150);

	this.instance_22 = new lib.i00044();
	this.instance_22.parent = this;
	this.instance_22.setTransform(-150,-150);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-150,300,300);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00051();
	this.instance.parent = this;
	this.instance.setTransform(10,-8,0.5,0.5,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-10,-8,20,16), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AAmAsQgCgCgBgIIgIgyIAAAAIgMAvQgCAHgDADQgDAFgHAAQgGAAgDgFQgDgCgCgIIgMgvIAAAAIgIAyQgBAIgCACQgDAFgGAAQgJAAAAgLIABgHIAKg/QABgHAFgFQAEgEAHAAQAGABAEADQAEADABAGIAMA0IAAAAIANg0QABgGAEgDQAFgDAFgBQAHAAAEAEQAFAFABAHIALA/IAAAHQAAALgJAAQgGAAgDgFg");
	this.shape.setTransform(66.7,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#013F5D").s().p("AANAhIAAgYIgYAgQgGAHgGAAQgEAAgDgCQgDgDAAgFQAAgFAGgGIARgTQgKgCgGgGQgGgHAAgKQAAgOAJgHQAJgJARAAIARAAQAIAAADADQADAEAAAIIAABBQAAAPgKAAQgLAAAAgPgAgKgQQAAAMANAAIAKAAIAAgXIgKAAQgNAAAAALg");
	this.shape_1.setTransform(57.2,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#013F5D").s().p("AgTAwQgIAAgEgDQgCgDAAgJIAAhBQAAgPAKAAQALAAAAAPIAAAQIAOAAQAPABAJAIQAJAIgBAPQABAPgKAJQgKAIgPAAgAgMAcIAJAAQAOAAAAgMQAAgNgOAAIgJAAg");
	this.shape_2.setTransform(49.2,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#013F5D").s().p("AgWAuQgMgEAAgKQAAgIAJAAIAIACIAHADIAJABQANAAAAgMQAAgEgDgEQgDgCgFgBIgHgBQgOABAAgJQAAgDAEgDQADgCAFgBIAIAAQAEAAADgBQAEgDAAgFQAAgKgMAAIgLACIgJACQgJAAAAgIQAAgDACgCQAIgJATAAQAhAAAAAbQAAANgNAGQAQAFAAAQQAAAdglABQgLgBgJgCg");
	this.shape_3.setTransform(41,-0.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#013F5D").s().p("AgRAvIABgGIAHgPIgbhCIgBgGQAAgFACgCQADgDAFAAQAGAAAEAJIARAuIABAAIARgtIADgHQADgDAFAAQAKAAgBAKIgBAHIgjBYIgCAGQgDACgEAAQgKAAAAgKg");
	this.shape_4.setTransform(33.6,0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#013F5D").s().p("AgiAhIAAhBQABgIADgEQADgDAIAAIATAAQAPAAAKAJQAJAIAAAPQAAAPgIAIQgJAJgQAAIgNAAIAAAQQAAAPgLAAQgLAAAAgPgAgMgCIAKAAQANAAAAgNQAAgMgNAAIgKAAg");
	this.shape_5.setTransform(25.9,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#013F5D").s().p("AAeA2QgCgDgBgEIAAgIIg2AAIAAAIQAAAEgCADQgDADgEAAQgEAAgEgDQgCgDAAgEIAAgRQAAgJAJAAQACAAABgFQACgEAAgJIAAgiQAAgLAGgHQAGgGAMgBIAgAAQAHABADAEQABADABAIIAAA9QALAAAAAJIAAARQAAAKgKAAQgEAAgDgDgAgNgfIAAAiQAAAJgDAJIAeAAIAAg6IgWAAQgFABAAAFg");
	this.shape_6.setTransform(16.7,0.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#013F5D").s().p("AgTAwQgIAAgEgDQgCgDAAgJIAAhBQAAgPAKAAQALAAAAAPIAAAQIAOAAQAPABAJAIQAJAIgBAPQABAPgKAJQgKAIgPAAgAgMAcIAJAAQAOAAAAgMQAAgNgOAAIgJAAg");
	this.shape_7.setTransform(3.7,-0.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#013F5D").s().p("AgJAhIAAg8IgPAAQgJgDAAgGQAAgLAQAAIAkAAQAPAAAAALQAAAGgIADIgPAAIAAA8QAAAPgLAAQgKAAABgPg");
	this.shape_8.setTransform(-3.8,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#013F5D").s().p("AAUAnIgEgMIggAAIgEAMQgDAKgHAAQgFgBgDgCQgDgDAAgEIABgHIAZhBQAFgOALgBQALAAAFANIAXBDIACAHQAAAEgDADQgDACgFABQgIgBgDgJgAgKAIIAUAAIgKggIAAAAg");
	this.shape_9.setTransform(-11.3,-0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#013F5D").s().p("AgWAuQgMgEAAgKQAAgIAJAAIAIACIAHADIAJABQANAAAAgMQAAgEgDgEQgDgCgFgBIgHgBQgOABAAgJQAAgDAEgDQADgCAFgBIAIAAQAEAAADgBQAEgDAAgFQAAgKgMAAIgLACIgJACQgJAAAAgIQAAgDACgCQAIgJATAAQAhAAAAAbQAAANgNAGQAQAFAAAQQAAAdglABQgLgBgJgCg");
	this.shape_10.setTransform(-19.3,-0.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#013F5D").s().p("AAUAnIgEgMIggAAIgEAMQgDAKgHAAQgFgBgDgCQgDgDAAgEIABgHIAZhBQAFgOALgBQALAAAFANIAXBDIACAHQAAAEgDADQgDACgFABQgIgBgDgJgAgKAIIAUAAIgKggIAAAAg");
	this.shape_11.setTransform(-27.2,-0.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#013F5D").s().p("AAQApIgbghIgEADIAAAWQAAAPgLABQgLgBAAgPIAAhBQAAgQALAAQALAAAAAQIAAAUIAagcQAIgHAEgBQAEAAAEADQADAEAAADQAAADgDAEIgGAFIgWAXIAdAfQAGAGAAAFQAAADgDADQgDAEgEAAQgFAAgHgIg");
	this.shape_12.setTransform(-35,-0.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#013F5D").s().p("AgZAjQgMgNAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAFQACADAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAJQgGAJAAAKQAAANAGAIQAGAIALAAQAGAAAGgDIAIgCQAEAAADADQACADAAAEQAAADgCADQgHAKgVAAQgUAAgNgPg");
	this.shape_13.setTransform(-43.7,-0.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#013F5D").s().p("AgZAjQgMgNAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAFQACADAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAJQgGAJAAAKQAAANAGAIQAGAIALAAQAGAAAGgDIAIgCQAEAAADADQACADAAAEQAAADgCADQgHAKgVAAQgUAAgNgPg");
	this.shape_14.setTransform(-52,-0.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#013F5D").s().p("AAUAnIgEgMIggAAIgEAMQgDAKgHAAQgFgBgDgCQgDgDAAgEIABgHIAZhBQAFgOALgBQALAAAFANIAXBDIACAHQAAAEgDADQgDACgFABQgIgBgDgJgAgKAIIAUAAIgKggIAAAAg");
	this.shape_15.setTransform(-60.3,-0.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#013F5D").s().p("AghAhIAAhBQAAgIACgEQAEgDAIAAIATAAQAPAAAKAJQAKAIgBAPQABAPgJAIQgJAJgPAAIgOAAIAAAQQAAAPgLAAQgKAAAAgPgAgMgCIAJAAQAOAAAAgNQAAgMgOAAIgJAAg");
	this.shape_16.setTransform(-68.3,0);

	this.instance = new lib.i00017();
	this.instance.parent = this;
	this.instance.setTransform(-110,-24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-110,-24,220,48), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgSAwQgJAAgEgDQgDgDAAgJIAAhBQAAgPALAAQALAAAAAPIAAAQIANAAQAQABAJAIQAJAIAAAPQAAAPgKAJQgJAIgQAAgAgMAcIAKAAQANAAAAgMQAAgNgNAAIgKAAg");
	this.shape.setTransform(34.6,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAhIAAg8IgQAAQgIgDAAgGQAAgLAPAAIAlAAQAPAAAAALQAAAGgIADIgPAAIAAA8QAAAPgLAAQgJAAAAgPg");
	this.shape_1.setTransform(27.1,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAUAhIAAgrIgnAzQgFAIgHAAQgJAAAAgNIAAhEQAAgQAKAAQALAAAAAQIAAArIAngzQAGgIAGAAQAKAAgBAPIAABCQABAPgLABQgLgBAAgPg");
	this.shape_2.setTransform(18.8,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAUAhIAAgYIgnAAIAAAYQAAAPgLABQgKgBAAgPIAAhBQAAgQAKAAQALAAAAAQIAAAWIAnAAIAAgWQAAgQALAAQAKAAABAQIAABBQgBAPgKABQgLgBAAgPg");
	this.shape_3.setTransform(8.8,-0.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAnIgEgMIggAAIgEAMQgDAKgHAAQgFgBgDgCQgDgDAAgEIABgHIAZhBQAFgOALgBQALAAAFANIAXBDIACAHQAAAEgDADQgDACgFABQgIgBgDgJgAgKAIIAUAAIgKggIAAAAg");
	this.shape_4.setTransform(-0.4,-0.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AghAhIAAhBQAAgIACgEQAEgDAJAAIASAAQAPAAAKAJQAKAIAAAPQAAAPgJAIQgJAJgPAAIgOAAIAAAQQAAAPgLAAQgKAAAAgPgAgMgCIAJAAQAOAAAAgNQAAgMgOAAIgJAAg");
	this.shape_5.setTransform(-8.3,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AATApIgTgbIgSAbQgGAHgFABQgEgBgDgCQgEgDABgDQAAgFAEgGIAYgfIgRgZQgFgHgBgEQAAgEAEgDQADgCAEgBQAFABAGAIIAMATIAOgTQAGgJAFAAQAEAAADADQADADAAAEQAAAEgFAHIgRAZIAXAfQAFAGAAAFQAAADgEADQgCACgEABQgGAAgGgIg");
	this.shape_6.setTransform(-16.5,-0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AghAiQgMgNABgVQgBgUAMgNQANgOAUgBQAVABANAOQAMANgBAUQABAVgMAOQgNAOgVAAQgUgBgNgOgAgRgTQgFAJAAAKQAAAMAFAIQAHAJAKAAQALAAAGgJQAGgIgBgMQAAgKgEgJQgHgIgLgBQgKABgHAIg");
	this.shape_7.setTransform(-25.2,-0.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgZAjQgMgNAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAFQACADAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAJQgGAJAAAKQAAANAGAIQAGAIALAAQAGAAAGgDIAIgCQAEAAADADQACADAAAEQAAADgCADQgHAKgVAAQgUAAgNgPg");
	this.shape_8.setTransform(-34.3,-0.1);

	this.instance = new lib.i00015();
	this.instance.parent = this;
	this.instance.setTransform(-73,-24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-73,-24,145,48), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer 1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Egj7AXcMAAAgu3MBH3AAAMAAAAu3g");
	mask.setTransform(0,150);

	// Layer 3
	this.instance = new lib.i00046();
	this.instance.parent = this;
	this.instance.setTransform(-359,0);

	this.instance_1 = new lib.i00045();
	this.instance_1.parent = this;
	this.instance_1.setTransform(736,-642);

	this.instance_2 = new lib.i00047();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-359,0);

	this.instance_3 = new lib.i00048();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-360,0);

	this.instance_4 = new lib.i00049();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-280,0);

	var maskedShapeInstanceList = [this.instance,this.instance_1,this.instance_2,this.instance_3,this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-230,0,460,300);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E50044").s().p("AAhBQIAAhKIhBBXQgJANgLAAQgRAAAAgWIAAh0QAAgaASAAQATAAAAAaIAABKIBChYQAJgMAKAAQARAAAAAZIAABxQAAAagSAAQgSAAgBgagAgahKQgKgIAAgKQAAgNAMAAQAGAAAFAIQAFAHAIAAQAJAAAFgHQAFgIAHAAQALAAAAANQAAANgNAHQgLAGgNAAQgPAAgLgIg");
	this.shape.setTransform(105.5,-2.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E50044").s().p("AAiA4IAAhJIhCBXQgJAMgLAAQgRAAAAgWIAAhzQAAgaASAAQASAAAAAaIAABKIBDhYQAJgMAKAAQARAAAAAYIAABxQAAAagSAAQgSAAAAgag");
	this.shape_1.setTransform(88.6,-0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E50044").s().p("Ag6A3IAAhuQAAgOAGgGQAFgFAOAAIAhAAQAbAAAPAOQAQAOAAAaQAAAagOAOQgPAOgbAAIgXAAIAAAbQAAAagTAAQgSAAAAgagAgVgEIAPAAQAZAAgBgWQABgVgZAAIgPAAg");
	this.shape_2.setTransform(73.8,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E50044").s().p("AgdBQQAAgGACgEIAKgaIgthvQgCgFAAgGQAAgIAFgFQAEgFAIAAQALAAAGAQIAfBOIAAAAIAdhNQAEgJACgDQAFgFAHAAQARAAAAARQAAAGgCAGIg7CWQgDAHgCADQgEAEgIAAQgQAAAAgRg");
	this.shape_3.setTransform(60.1,1.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E50044").s().p("AAbBFIgwg4IgFAFIAAAmQgBAagSAAQgSAAAAgaIAAhvQAAgaASAAQASAAABAaIAAAjIAtgwQANgNAHAAQAHAAAFAFQAGAFAAAHQAAAFgGAGIgJAJIglAlIAyA2QAJALAAAHQAAAHgGAFQgEAFgHAAQgJAAgLgNg");
	this.shape_4.setTransform(47.4,-0.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E50044").s().p("Ag5A3IAAhuQgBgOAGgGQAFgFAPAAIAgAAQAaAAAQAOQARAOAAAaQgBAagPAOQgPAOgZAAIgYAAIAAAbQAAAagSAAQgTAAABgagAgVgEIAQAAQAXAAAAgWQAAgVgXAAIgQAAg");
	this.shape_5.setTransform(33.1,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E50044").s().p("AgVBQQgOAAgFgEQgFgFgBgOIAAhvQABgOAFgGQAFgFAPAAIAoAAQAZAAAAARQAAARgZAAIgdAAIAAAcIAYAAQAbAAAAARQAAAQgbAAIgYAAIAAAeIAeAAQAZAAAAARQAAARgZAAg");
	this.shape_6.setTransform(20.5,-0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E50044").s().p("ABBBLQgDgFgCgLIgOhXIgVBQQgDANgFAFQgFAIgMAAQgKAAgGgIQgEgFgDgNIgWhQIAAAAIgNBXQgCALgEAFQgEAHgJAAQgRAAAAgRIABgNIAShsQACgLAHgHQAIgHALAAQAKAAAHAGQAHAGACAJIAVBZIABAAIAVhZQACgJAHgGQAHgGAKAAQALAAAIAHQAIAHABALIASBsIABANQAAARgQAAQgJAAgFgHg");
	this.shape_7.setTransform(4.5,-0.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E50044").s().p("AAiBBIgGgTIg4AAIgHAUQgFAQgMAAQgIABgFgGQgFgEAAgIQAAgGACgEIAqhxQAJgXASAAQATAAAIAVIAoBzQADAGAAAEQAAAIgFAFQgGAEgHAAQgOAAgFgRgAgSAOIAkAAIgSg4IAAAAg");
	this.shape_8.setTransform(-19.9,-0.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E50044").s().p("AgRA3IAAhlQgUAAgFgBQgPgDAAgNQAAgRAaAAIA/AAQAaAAAAARQAAANgPADQgFABgUAAIAABlQAAAagSAAQgRAAAAgag");
	this.shape_9.setTransform(-32.4,0);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E50044").s().p("AgVBQQgOAAgFgEQgGgFABgOIAAhvQgBgOAGgGQAFgFAPAAIAoAAQAaAAgBARQABARgaAAIgdAAIAAAcIAZAAQAaAAAAARQAAAQgaAAIgZAAIAAAeIAeAAQAaAAgBARQABARgaAAg");
	this.shape_10.setTransform(-43.2,-0.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E50044").s().p("AAhA4IAAgpIhBAAIAAApQgBAagSAAQgSAAAAgaIAAhvQAAgaASAAQASAAABAaIAAAlIBBAAIAAglQAAgaATAAQASAAAAAaIAABvQAAAagSAAQgTAAAAgag");
	this.shape_11.setTransform(-57.8,-0.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E50044").s().p("AAiBBIgGgTIg4AAIgHAUQgFAQgMAAQgIABgFgGQgFgEAAgIQAAgGACgEIAqhxQAJgXASAAQATAAAIAVIAoBzQADAGAAAEQAAAIgFAFQgGAEgHAAQgOAAgFgRgAgSAOIAkAAIgSg4IAAAAg");
	this.shape_12.setTransform(-73.3,-0.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E50044").s().p("AAnBKQgDgFAAgNIAAhpIgmAAQgJAAAAAKIAABZQAAAggfAAQgdAAAAgRQgBgKAGgDQADgCAIAAQAIgBACgCQACgCABgHIAAhNQAAgYAIgJQAJgJAXAAIA4AAQASAAABAaIAABvQgBAagSAAQgLAAgEgIg");
	this.shape_13.setTransform(-89,-0.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E50044").s().p("AAhA3IAAhnIhCAAIAABnQABAagTAAQgSAAAAgaIAAhtQAAgaAZAAIBZAAQAZAAAAAaIAABtQAAAagSAAQgSAAgBgag");
	this.shape_14.setTransform(-105.5,0);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E50044").s().p("AgVBQQgOAAgFgEQgFgFgBgOIAAhvQABgOAFgGQAFgFAPAAIAoAAQAZAAAAARQAAARgZAAIgdAAIAAAcIAZAAQAaAAAAARQAAAQgaAAIgZAAIAAAeIAeAAQAZAAAAARQAAARgZAAg");
	this.shape_15.setTransform(61.9,-0.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E50044").s().p("AAhA4IAAgpIhBAAIAAApQAAAagTAAQgSAAAAgaIAAhvQAAgaASAAQATAAAAAaIAAAlIBBAAIAAglQABgaASAAQASAAAAAaIAABvQAAAagSAAQgSAAgBgag");
	this.shape_16.setTransform(47.4,-0.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E50044").s().p("AgVBQQgOAAgFgEQgGgFABgOIAAhvQgBgOAGgGQAFgFAOAAIApAAQAZAAABARQgBARgZAAIgdAAIAAAcIAZAAQAZAAAAARQAAAQgZAAIgZAAIAAAeIAeAAQAaAAgBARQABARgaAAg");
	this.shape_17.setTransform(33.6,-0.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E50044").s().p("AgiBQQgNAAgGgEQgFgFAAgOIAAhvQAAgOAFgGQAGgFAOAAIAeAAQAVAAAPAJQATAKAAAVQgBAagSAJIAAABQANADAHAJQAGAIAAAPQAAAYgRANQgPALgaAAgAgVAxIASAAQALAAAFgDQAIgFAAgKQAAgNgIgEQgFgDgMAAIgRAAgAgVgRIAPAAQAKAAAEgCQAHgEAAgKQAAgPgVAAIgPAAg");
	this.shape_18.setTransform(21,-0.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E50044").s().p("AgVBQQgOAAgFgEQgGgFAAgOIAAhvQAAgOAGgGQAFgFAOAAIApAAQAaAAAAARQAAARgaAAIgdAAIAAAcIAYAAQAaAAAAARQAAAQgaAAIgYAAIAAAeIAeAAQAaAAAAARQAAARgaAAg");
	this.shape_19.setTransform(-24.3,-0.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E50044").s().p("AAiA4IAAgpIhDAAIAAApQAAAagSAAQgSAAAAgaIAAhvQAAgaASAAQASAAAAAaIAAAlIBDAAIAAglQAAgaASAAQASAAAAAaIAABvQAAAagSAAQgSAAAAgag");
	this.shape_20.setTransform(-38.8,-0.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E50044").s().p("AAiBBIgGgTIg4AAIgHAUQgFAQgMAAQgIABgFgGQgFgEAAgIQAAgGACgEIAqhxQAJgXASAAQATAAAIAVIAoBzQADAGAAAEQAAAIgFAFQgGAEgHAAQgOAAgFgRgAgSAOIAkAAIgSg4IAAAAg");
	this.shape_21.setTransform(-54.3,-0.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E50044").s().p("AAnBKQgDgFAAgNIAAhpIgmAAQgIAAAAAKIAABZQAAAgghAAQgdAAAAgRQABgKAFgDQADgCAIAAQAIgBACgCQACgCAAgHIAAhNQAAgYAJgJQAJgJAXAAIA4AAQASAAAAAaIAABvQAAAagSAAQgLAAgEgIg");
	this.shape_22.setTransform(-70,-0.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#E50044").s().p("AAhA3IAAhnIhBAAIAABnQgBAagSAAQgSAAAAgaIAAhtQAAgaAZAAIBZAAQAZAAAAAaIAABtQAAAagSAAQgTAAAAgag");
	this.shape_23.setTransform(-86.6,0);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E50044").s().p("AgVBQQgOAAgFgEQgGgFAAgOIAAhvQAAgOAGgGQAFgFAOAAIApAAQAaAAAAARQAAARgaAAIgdAAIAAAcIAYAAQAaAAAAARQAAAQgaAAIgYAAIAAAeIAeAAQAZAAABARQgBARgZAAg");
	this.shape_24.setTransform(34.3,-0.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E50044").s().p("AgRA3IAAhlQgUAAgFgBQgPgDAAgNQAAgRAaAAIA/AAQAaAAAAARQAAANgPADQgFABgUAAIAABlQAAAagSAAQgRAAAAgag");
	this.shape_25.setTransform(-15.8,0);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#E50044").s().p("Ag6A3IAAhuQAAgOAGgGQAFgFAOAAIAhAAQAbAAAPAOQAQAOAAAaQAAAagOAOQgPAOgbAAIgXAAIAAAbQAAAagTAAQgSAAAAgagAgVgEIAPAAQAZAAAAgWQAAgVgZAAIgPAAg");
	this.shape_26.setTransform(72.3,0);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E50044").s().p("AgrA9QgUgYgBgkQAAgkAWgYQAWgZAjAAQAMABAMAEQAOADAHAJQADAEAAAFQAAAGgFAGQgFAEgGAAQgGAAgIgCQgIgEgKAAQgTAAgKAPQgJAOAAAUQgBAUAKAOQALAOATAAQAJAAAKgDQAMgEADgBQAGABAFAEQADAFAAAGQAAAGgDAEQgMASgjgBQgjAAgWgXg");
	this.shape_27.setTransform(20.1,-0.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E50044").s().p("AAiA4IAAgpIhDAAIAAApQAAAagSAAQgSAAAAgaIAAhvQAAgaASAAQASAAAAAaIAAAlIBDAAIAAglQAAgaASAAQASAAAAAaIAABvQAAAagSAAQgSAAAAgag");
	this.shape_28.setTransform(-39.4,-0.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E50044").s().p("AAnBKQgDgFAAgNIAAhpIgmAAQgJAAAAAKIAABZQAAAggfAAQgdAAgBgRQAAgKAGgDQADgCAIAAQAIgBACgCQADgCAAgHIAAhNQgBgYAJgJQAJgJAXAAIA4AAQASAAABAaIAABvQgBAagSAAQgLAAgEgIg");
	this.shape_29.setTransform(-70.6,-0.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E50044").s().p("AAhA3IAAhnIhBAAIAABnQAAAagTAAQgSAAAAgaIAAhtQAAgaAZAAIBZAAQAZAAAAAaIAABtQAAAagSAAQgSAAgBgag");
	this.shape_30.setTransform(-87.2,0);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E50044").s().p("AgrA9QgVgYABgkQAAgkAVgYQAWgZAiAAQANABAMAEQAOADAGAJQAEAEAAAFQAAAGgFAGQgFAEgGAAQgGAAgIgCQgJgEgJAAQgTAAgLAPQgJAOAAAUQAAAUAKAOQALAOATAAQAJAAALgDQALgEADgBQAGABAFAEQADAFAAAGQAAAGgDAEQgMASgjgBQgjAAgWgXg");
	this.shape_31.setTransform(78.2,-0.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E50044").s().p("Ag5A3IAAhuQAAgOAFgGQAFgFAPAAIAgAAQAaAAAQAOQARAOAAAaQgBAagPAOQgOAOgaAAIgYAAIAAAbQAAAagSAAQgSAAAAgagAgVgEIAQAAQAYAAgBgWQABgVgYAAIgQAAg");
	this.shape_32.setTransform(64.7,0);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E50044").s().p("ABBBLQgDgFgCgLIgOhXIgVBQQgDANgEAFQgHAIgLAAQgKAAgGgIQgEgFgDgNIgVhQIgBAAIgOBXQgBALgEAFQgEAHgJAAQgRAAAAgRIABgNIAShsQACgLAHgHQAIgHALAAQAKAAAHAGQAHAGACAJIAVBZIABAAIAVhZQACgJAHgGQAHgGAKAAQALAAAIAHQAIAHABALIASBsIABANQAAARgQAAQgJAAgFgHg");
	this.shape_33.setTransform(33.4,-0.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E50044").s().p("AgVBQQgOAAgFgEQgFgFAAgOIAAhvQAAgOAFgGQAFgFAPAAIAoAAQAaAAgBARQABARgaAAIgdAAIAAAcIAZAAQAaAAAAARQAAAQgaAAIgZAAIAAAeIAeAAQAaAAgBARQABARgaAAg");
	this.shape_34.setTransform(-14.3,-0.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E50044").s().p("AAnBKQgDgFAAgNIAAhpIgmAAQgJAAAAAKIAABZQAAAggfAAQgdAAAAgRQgBgKAGgDQADgCAJAAQAHgBACgCQACgCABgHIAAhNQAAgYAIgJQAIgJAYAAIA4AAQATAAAAAaIAABvQAAAagTAAQgLAAgEgIg");
	this.shape_35.setTransform(-60.1,-0.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E50044").s().p("AAiA3IAAhnIhDAAIAABnQAAAagSAAQgSAAAAgaIAAhtQAAgaAZAAIBZAAQAZAAAAAaIAABtQAAAagSAAQgSAAAAgag");
	this.shape_36.setTransform(-76.6,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14,p:{x:-105.5}},{t:this.shape_13,p:{x:-89}},{t:this.shape_12,p:{x:-73.3}},{t:this.shape_11,p:{x:-57.8}},{t:this.shape_10,p:{x:-43.2}},{t:this.shape_9,p:{x:-32.4}},{t:this.shape_8,p:{x:-19.9}},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5,p:{x:33.1}},{t:this.shape_4},{t:this.shape_3,p:{x:60.1}},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_23,p:{x:-86.6}},{t:this.shape_22},{t:this.shape_21,p:{x:-54.3}},{t:this.shape_20,p:{x:-38.8}},{t:this.shape_19},{t:this.shape_9,p:{x:-13.5}},{t:this.shape_12,p:{x:-0.9}},{t:this.shape_18},{t:this.shape_17,p:{x:33.6}},{t:this.shape_16,p:{x:47.4}},{t:this.shape_15},{t:this.shape_5,p:{x:74.5}},{t:this.shape_8,p:{x:88}}]},1).to({state:[{t:this.shape_14,p:{x:-88.9}},{t:this.shape_13,p:{x:-72.3}},{t:this.shape_12,p:{x:-56.6}},{t:this.shape_11,p:{x:-41.1}},{t:this.shape_10,p:{x:-26.6}},{t:this.shape_25,p:{x:-15.8}},{t:this.shape_8,p:{x:-3.2}},{t:this.shape_20,p:{x:19.8}},{t:this.shape_24},{t:this.shape_23,p:{x:48.1}},{t:this.shape_9,p:{x:62}},{t:this.shape_3,p:{x:74.1}},{t:this.shape_16,p:{x:88.9}}]},1).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_21,p:{x:-54.9}},{t:this.shape_28},{t:this.shape_17,p:{x:-24.9}},{t:this.shape_25,p:{x:-14.1}},{t:this.shape_12,p:{x:-1.5}},{t:this.shape_27},{t:this.shape_8,p:{x:34.1}},{t:this.shape_9,p:{x:46.6}},{t:this.shape_3,p:{x:58.7}},{t:this.shape_26},{t:this.shape_20,p:{x:87.2}}]},1).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_21,p:{x:-44.4}},{t:this.shape_11,p:{x:-28.9}},{t:this.shape_34},{t:this.shape_9,p:{x:-3.5}},{t:this.shape_12,p:{x:9}},{t:this.shape_33},{t:this.shape_8,p:{x:50.4}},{t:this.shape_32},{t:this.shape_31}]},1).wait(1));

	// Layer 3
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#0F4E6A").s().p("AAgAzIgIgWIgwAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAHAAADAIIAjBeIABADQgBAGgGAAQgFAAgCgFgAgUARIAoAAIgUg4g");
	this.shape_37.setTransform(136.6,28.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#0F4E6A").s().p("AgxA3QgDAAgBgCQgCgCAAgDIAAhfQAAgDACgCQAAgBABAAQAAAAABgBQAAAAABAAQAAAAABAAQAHAAAAAHIAABbIAkAAIAAhbQAAgDACgCQABgBAAAAQABAAAAgBQABAAABAAQAAAAAAAAQAHAAAAAHIAABbIAkAAIAAhbQAAgHAHAAQAGAAAAAHIAABfQAAADgBACQgCACgDAAg");
	this.shape_38.setTransform(125.3,28.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#0F4E6A").s().p("AAeAwIAAhfQAAgIAHAAQAGAAABAIIAABfQgBAIgGAAQgHAAAAgIgAgkA3QgHAAAAgHIAAhfQABgIAGAAQADAAACADQACACAAADIAAAlIAQAAQAHAAAGACQAIAEAGAHQAGAJgBAKQAAAPgKAJQgJAJgPAAgAgdArIANAAQAKAAAGgGQAGgFAAgKQAAgKgGgGQgGgFgKAAIgNAAg");
	this.shape_39.setTransform(113.6,28.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgGAGQgFAFgMAAQgJAAAAgGQABgFAFAAIAIgBQAFgCAAgJIAAhFQAAgRASAAIApAAQAHAAAAAHIAABfQAAADgCACQgDACgCAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_40.setTransform(103.1,28.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#0F4E6A").s().p("AAgAzIgIgWIgwAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAHAAADAIIAjBeIABADQgBAGgGAAQgFAAgCgFgAgUARIAoAAIgUg4g");
	this.shape_41.setTransform(94.3,28.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#0F4E6A").s().p("AgGAyIgahYIgLBXQgBAHgHAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAQgCgCAAgEIANheQABgEADgCQADgDAFAAQAHAAADAHIAWBPIAAAAIAXhPQADgHAHAAQAFAAADADQADACABAEIAMBeQABAEgCACQAAAAgBABQAAAAgBAAQAAAAgBAAQAAABgBAAQgHAAgBgHIgLhXIgaBYQgCAGgFAAQgEAAgCgGg");
	this.shape_42.setTransform(83.4,28.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#0F4E6A").s().p("AgjAnQgMgQAAgXQAAgWAMgQQANgRAWAAQAWAAAOARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgOgRgAgZgdQgIAMABARQgBASAIAMQAJANAQAAQAMAAAJgHQAIgIADgMQACgHgBgJQAAgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_43.setTransform(67.8,28.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#0F4E6A").s().p("AgWA1QgCgCAAgDIAAhfQAAgDACgCQAAAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAIAmAAQAGAAAAAGQAAAFgGAAIgeAAIAABbQABAHgIAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBg");
	this.shape_44.setTransform(59.9,28.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#0F4E6A").s().p("AgjAnQgMgQAAgXQAAgWAMgQQANgRAWAAQAXAAANARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgOgRgAgZgdQgIAMABARQgBASAIAMQAJANAQAAQAMAAAJgHQAHgIAEgMQABgHAAgJQAAgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_45.setTransform(51.2,28.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#0F4E6A").s().p("AgaA3QgHAAAAgHIAAhfQgBgHAIAAIAYAAQAKAAAJAHQAJAIAAALQAAAIgEAHQgDAHgIACIAAABQALACAGAGQAHAHAAALQAAAOgLAJQgKAJgOAAgAgUArIASAAQAJAAAHgFQAHgFAAgKQAAgKgHgFQgGgFgIAAIgUAAgAgUgIIAPAAQAHAAAGgEQAEgFAAgIQAAgHgEgFQgFgFgGAAIgRAAg");
	this.shape_46.setTransform(42.1,28.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#0F4E6A").s().p("AAdAxIAAhQIg5BSQgDAFgEAAQgGAAAAgHIAAhgQAAgIAHAAQABAAAAAAQABABAAAAQABAAAAAAQABABAAAAQACADAAADIAABOIA4hQQAEgGAEAAQAGAAAAAHIAABhQAAAHgGAAQgHAAAAgHg");
	this.shape_47.setTransform(32.6,28.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgFAGQgGAFgMAAQgIAAgBgGQAAgFAGAAIAIgBQAFgCAAgJIAAhFQAAgRASAAIAqAAQAFAAAAAHIAABfQAAADgCACQgCACgCAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_48.setTransform(22.3,28.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#0F4E6A").s().p("AAlA/QgCgBAAgDIAAgPIhBAAQAAAAgBAAQAAAAgBAAQAAABgBAAQAAAAAAAAIgBAEIAAAKQAAADgCABQgCACgCAAQgDAAgDgCQgBgBAAgDIAAgSQgBgGAJgCQAIgDAAgOIAAg/QAAgRASAAIAsAAQAGAAAAAHIAABaIAHAAQAFABAAAEIAAAVQAAADgCABQgDACgDAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBgAgSguIAAA+QAAAKgEAHIAtAAIAAhWIgjAAQgGAAAAAHg");
	this.shape_49.setTransform(13.4,29.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#0F4E6A").s().p("AgTA3QgGAAgBgHIAAheQABgDABgCQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAIAoAAQAHAAgBAHQABAGgHAAIggAAIAAAhIAeAAQAHAAAAAGQAAAFgHAAIgeAAIAAAnIAgAAQAHAAgBAHQABAGgIAAg");
	this.shape_50.setTransform(5.5,28.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#0F4E6A").s().p("AgcAnQgMgQAAgXQAAgWAMgQQANgRAWAAQASAAALALQADADAAADQAAABAAAAQAAABAAAAQgBABAAAAQAAABgBAAIgEACQgCAAgDgDIgGgEIgGgDIgKgBQgOAAgKAPQgIANAAAPQAAARAIAMQAJAOAPAAQALAAAIgEIAFgDQAEgCACAAQAFAAAAAGQAAADgCACQgFAGgKACQgJADgIAAQgWAAgNgRg");
	this.shape_51.setTransform(-2.6,28.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#0F4E6A").s().p("AgiAnQgNgQAAgXQAAgWANgQQAMgRAWAAQAXAAANARQAMAQAAAWQAAAXgMAQQgOARgWAAQgVAAgNgRgAgZgdQgHAMAAARQAAASAHAMQAJANAQAAQAMAAAJgHQAHgIADgMQACgHAAgJQABgRgIgMQgJgNgRAAQgQAAgJANg");
	this.shape_52.setTransform(-12.5,28.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#0F4E6A").s().p("AAcA1QgCgCAAgDIAAhbIgzAAIAABbQAAAHgHAAQgBAAAAAAQgBAAgBAAQAAAAgBgBQAAAAgBgBQgCgCAAgDIAAhfQAAgDACgCQAAAAABgBQAAAAAAgBQABAAABAAQAAAAABAAIBDAAQAGAAAAAHIAABfQAAADgCACQgCACgDAAQgDAAgCgCg");
	this.shape_53.setTransform(-22.6,28.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#0F4E6A").s().p("AgTA3QgGAAAAgHIAAheQgBgDACgCQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAIAoAAQAHAAAAAHQAAAGgHAAIggAAIAAAhIAfAAQAFAAABAGQgBAFgFAAIgfAAIAAAnIAgAAQAHAAAAAHQAAAGgIAAg");
	this.shape_54.setTransform(-30.7,28.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#0F4E6A").s().p("AAeA1QgCgCAAgDIAAgtIg3AAIAAAtQAAAIgHAAQgGAAAAgIIAAhfQAAgDACgCQABgDADAAQAHAAAAAIIAAAnIA3AAIAAgnQAAgIAGAAQAEAAABADQACACAAADIAABfQAAAIgHAAQgDAAgBgDg");
	this.shape_55.setTransform(-39.3,28.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#0F4E6A").s().p("AgiAnQgNgQAAgXQAAgWANgQQAMgRAWAAQAXAAANARQAMAQAAAWQAAAXgMAQQgOARgWAAQgVAAgNgRgAgZgdQgHAMAAARQAAASAHAMQAJANAQAAQAMAAAJgHQAHgIADgMQACgHAAgJQABgRgIgMQgJgNgRAAQgQAAgJANg");
	this.shape_56.setTransform(-53.9,28.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#0F4E6A").s().p("AgWA1QgCgCAAgDIAAhfQAAgDABgCQABAAAAgBQABAAAAgBQABAAAAAAQABAAABAAIAlAAQAGAAAAAGQAAAFgGAAIgdAAIAABbQgBAHgGAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBg");
	this.shape_57.setTransform(-61.8,28.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#0F4E6A").s().p("AgiAnQgNgQAAgXQAAgWANgQQANgRAVAAQAXAAANARQAMAQAAAWQAAAXgMAQQgOARgWAAQgVAAgNgRgAgZgdQgHAMgBARQABASAHAMQAJANAQAAQAMAAAIgHQAJgIACgMQACgHAAgJQABgRgIgMQgJgNgRAAQgQAAgJANg");
	this.shape_58.setTransform(-70.4,28.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#0F4E6A").s().p("AgGAyIgahYIgLBXQgBAHgHAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAQgCgCAAgEIANheQABgEADgCQADgDAFAAQAHAAADAHIAWBPIAAAAIAXhPQADgHAHAAQAFAAADADQADACABAEIAMBeQABAEgCACQAAAAgBABQAAAAgBAAQAAAAgBAAQAAABgBAAQgHAAgBgHIgLhXIgaBYQgCAGgFAAQgEAAgCgGg");
	this.shape_59.setTransform(-81.8,28.4);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#0F4E6A").s().p("AAhAzIgIgWIgxAAIgIAWQgBAFgFAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAgBQgBAAAAgBIABgDIAjheQADgIAGAAQAGAAADAIIAkBeIAAADQAAAGgGAAQgFAAgBgFgAgTARIAnAAIgUg4g");
	this.shape_60.setTransform(-92.6,28.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#0F4E6A").s().p("AgcAnQgMgQAAgXQAAgWAMgQQANgRAWAAQASAAALALQADADAAADQAAABAAAAQAAABAAAAQAAABgBAAQAAABgBAAIgEACQgCAAgDgDIgGgEIgGgDIgKgBQgOAAgKAPQgIANAAAPQAAARAIAMQAJAOAPAAQALAAAIgEIAFgDQAEgCACAAQAFAAAAAGQAAADgCACQgFAGgKACQgJADgIAAQgWAAgNgRg");
	this.shape_61.setTransform(-101.9,28.3);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#0F4E6A").s().p("AAUAxIAAgtIgBAAIgnAvQgDAEgDAAQgHAAAAgFQAAgEADgDIAegkQgOAAgIgIQgJgIAAgOQAAgOAJgJQAKgIANAAIAaAAQAHAAgBAHIAABfQABAHgHAAQgHAAAAgGgAgMglQgGAFABAJQgBALAGAFQAFAEAKAAQAKAAAHgCIAAgmIgSAAQgIAAgGAGg");
	this.shape_62.setTransform(-115,28.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgGAGQgFAFgMAAQgJAAAAgGQAAgFAGAAIAIgBQAFgCAAgJIAAhFQAAgRASAAIAqAAQAFAAABAHIAABfQgBADgCACQgCACgCAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_63.setTransform(-123.9,28.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#0F4E6A").s().p("AAlA/QgCgBAAgDIAAgPIhBAAQAAAAgBAAQAAAAgBAAQAAABgBAAQAAAAAAAAIgBAEIAAAKQAAADgCABQgCACgCAAQgEAAgCgCQgCgBAAgDIAAgSQAAgGAIgCQAJgDAAgOIAAg/QAAgRASAAIAsAAQAGAAAAAHIAABaIAHAAQAFABAAAEIAAAVQAAADgCABQgCACgEAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBgAgSguIAAA+QAAAKgEAHIAtAAIAAhWIgjAAQgGAAAAAHg");
	this.shape_64.setTransform(-132.7,29.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#0F4E6A").s().p("AAgAzIgIgWIgwAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAHAAADAIIAjBeIABADQgBAGgGAAQgFAAgCgFgAgUARIApAAIgVg4g");
	this.shape_65.setTransform(144.2,28.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgGAGQgFAFgMAAQgJAAAAgGQABgFAFAAIAIgBQAFgCAAgJIAAhFQAAgRASAAIApAAQAHAAAAAHIAABfQAAADgCACQgDACgDAAQAAAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_66.setTransform(110.8,28.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#0F4E6A").s().p("AgjAnQgMgQAAgXQAAgWAMgQQANgRAWAAQAWAAAOARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgOgRgAgZgdQgIAMABARQgBASAIAMQAJANAQAAQAMAAAJgHQAIgIADgMQABgHAAgJQAAgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_67.setTransform(75.4,28.3);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#0F4E6A").s().p("AgaA3QgHAAAAgHIAAheQAAgIAHAAQADAAACACQACACAAAEIAAAlIATAAQAOAAAKAJQAKAIAAAOQAAAQgKAJQgKAIgPAAgAgTAsIASAAQAJAAAGgGQAGgGAAgKQAAgKgGgFQgHgGgJAAIgRAAg");
	this.shape_68.setTransform(39.7,28.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgFAGQgGAFgMAAQgIAAAAgGQAAgFAEAAIAJgBQAFgCAAgJIAAhFQAAgRASAAIAqAAQAFAAAAAHIAABfQAAADgCACQgBACgDAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_69.setTransform(30.3,28.4);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#0F4E6A").s().p("AgTA3QgHAAAAgHIAAheQAAgDACgCQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAIApAAQAFAAAAAHQAAAGgFAAIghAAIAAAhIAeAAQAHAAgBAGQABAFgHAAIgeAAIAAAnIAhAAQAFAAAAAHQAAAGgGAAg");
	this.shape_70.setTransform(22.9,28.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#0F4E6A").s().p("AgEA1QgCgCAAgDIAAhbIgVAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAgBQAAgBAAAAQAAgBAAAAQAAgBABgBQAAAAAAgBQABAAAAgBQAAAAABAAQABgBAAAAQABAAAAAAQABAAABAAIA1AAQAHAAAAAGQAAAFgGAAIgVAAIAABbQAAADgCACQgBABAAAAQgBABAAAAQgBAAgBAAQAAAAgBAAQgCAAgCgCg");
	this.shape_71.setTransform(15.8,28.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#0F4E6A").s().p("AAhAzIgIgWIgxAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAHAAADAIIAjBeIAAADQAAAGgGAAQgFAAgBgFgAgTARIAnAAIgUg4g");
	this.shape_72.setTransform(7.6,28.3);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#0F4E6A").s().p("AgaA3QgIAAABgHIAAhfQAAgHAGAAIAZAAQAKAAAJAHQAJAIAAALQAAAIgDAHQgEAHgIACIAAABQALACAGAGQAGAHAAALQAAAOgKAJQgKAJgOAAgAgUArIASAAQAJAAAHgFQAHgFAAgKQAAgKgHgFQgFgFgKAAIgTAAgAgUgIIAQAAQAGAAAFgEQAGgFAAgIQgBgHgEgFQgFgFgGAAIgRAAg");
	this.shape_73.setTransform(-1,28.3);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#0F4E6A").s().p("AggA1QgCgCAAgDIAAhfQAAgHAHAAIAbAAQAPAAAKAJQAKAKAAAPQAAAOgKAJQgKAKgOAAIgVAAIAAAkQAAAGgHAAQgBAAAAAAQgBAAgBAAQAAAAgBgBQAAAAgBgBgAgUACIATAAQAJAAAHgGQAGgGAAgKQAAgKgGgGQgGgHgJAAIgUAAg");
	this.shape_74.setTransform(-19.5,28.4);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#0F4E6A").s().p("AAhAzIgJgWIgwAAIgIAWQgBAFgFAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAgBQgBAAAAgBIABgDIAjheQADgIAGAAQAGAAADAIIAkBeIAAADQABAGgHAAQgFAAgBgFgAgUARIApAAIgVg4g");
	this.shape_75.setTransform(-28.5,28.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#0F4E6A").s().p("AAWA2QAAgBgBAAQAAgBAAAAQAAgBgBgBQAAAAAAgBIAAgmQgLADgJAAQgPAAgJgJQgJgIAAgRIAAgbQAAgIAHAAQAHAAAAAIIAAAbQAAAWATAAQAMAAAIgCIAAgvQAAgIAHAAQAHAAAAAIIAABgQAAADgCACQgBAAAAABQgBAAAAAAQgBABgBAAQAAAAgBAAQgDAAgCgCg");
	this.shape_76.setTransform(-37.5,28.3);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#0F4E6A").s().p("AgjAnQgMgQAAgXQAAgWAMgQQANgRAWAAQAWAAAOARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgOgRgAgZgdQgIAMAAARQAAASAIAMQAJANAQAAQAMAAAIgHQAIgIAEgMQACgHAAgJQAAgRgIgMQgJgNgRAAQgQAAgJANg");
	this.shape_77.setTransform(-46.7,28.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#0F4E6A").s().p("AgiAnQgNgQAAgXQAAgWANgQQANgRAVAAQAXAAANARQAMAQAAAWQAAAXgMAQQgOARgWAAQgVAAgNgRgAgZgdQgHAMgBARQABASAHAMQAJANAQAAQAMAAAIgHQAJgIACgMQACgHAAgJQABgRgIgMQgJgNgRAAQgQAAgJANg");
	this.shape_78.setTransform(-78.1,28.3);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#0F4E6A").s().p("AAgAzIgIgWIgwAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAgBQgBAAAAgBIABgDIAjheQAEgIAFAAQAGAAAEAIIAjBeIABADQAAAGgHAAQgFAAgCgFgAgUARIApAAIgVg4g");
	this.shape_79.setTransform(111.6,28.3);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#0F4E6A").s().p("AgxA3QgDAAgBgCQgCgCAAgDIAAhfQAAgDACgCQAAgBABAAQAAAAABgBQAAAAABAAQABAAAAAAQAHAAAAAHIAABbIAkAAIAAhbQAAgDACgCQABgBAAAAQABAAAAgBQABAAABAAQAAAAAAAAQAHAAAAAHIAABbIAkAAIAAhbQAAgHAHAAQAGAAAAAHIAABfQAAADgBACQgCACgDAAg");
	this.shape_80.setTransform(100.4,28.3);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#0F4E6A").s().p("AAeAwIAAhfQAAgIAHAAQAGAAABAIIAABfQgBAIgGAAQgHAAAAgIgAgkA3QgGAAgBgHIAAhfQAAgIAIAAQADAAABADQACACAAADIAAAlIAQAAQAHAAAGACQAJAEAFAHQAFAJAAAKQABAPgLAJQgJAJgPAAgAgdArIANAAQALAAAFgGQAGgFAAgKQAAgKgGgGQgGgFgKAAIgNAAg");
	this.shape_81.setTransform(88.6,28.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#0F4E6A").s().p("AAgAzIgIgWIgwAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAgBQgBAAAAgBIABgDIAjheQAEgIAFAAQAGAAAEAIIAjBeIABADQAAAGgHAAQgFAAgCgFgAgUARIApAAIgVg4g");
	this.shape_82.setTransform(69.3,28.3);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#0F4E6A").s().p("AgWA1QgCgCAAgDIAAhfQAAgDACgCQAAAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAIAlAAQAHAAAAAGQAAAFgHAAIgcAAIAABbQAAAHgIAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBg");
	this.shape_83.setTransform(34.9,28.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#0F4E6A").s().p("AgjAnQgMgQAAgXQAAgWAMgQQANgRAWAAQAWAAAOARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgOgRgAgZgdQgIAMAAARQAAASAIAMQAJANAQAAQAMAAAIgHQAIgIAEgMQACgHAAgJQgBgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_84.setTransform(26.3,28.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#0F4E6A").s().p("AgTA3QgHAAABgHIAAheQAAgDABgCQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAIApAAQAFAAABAHQgBAGgFAAIghAAIAAAhIAfAAQAFAAAAAGQAAAFgFAAIgfAAIAAAnIAhAAQAFAAABAHQgBAGgGAAg");
	this.shape_85.setTransform(-7.5,28.3);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#0F4E6A").s().p("AgaA3QgHAAgBgHIAAhfQAAgHAIAAIAYAAQALAAAIAHQAJAIAAALQAAAIgEAHQgEAHgHACIAAABQAKACAHAGQAHAHAAALQAAAOgLAJQgKAJgOAAgAgUArIASAAQAJAAAHgFQAHgFAAgKQAAgKgHgFQgGgFgIAAIgUAAgAgUgIIAPAAQAHAAAGgEQAEgFAAgIQABgHgFgFQgFgFgGAAIgRAAg");
	this.shape_86.setTransform(-15,28.3);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#0F4E6A").s().p("AgWA1QgCgCAAgDIAAhfQAAgDABgCQABAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAIAmAAQAGAAAAAGQAAAFgGAAIgeAAIAABbQAAAHgGAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBg");
	this.shape_87.setTransform(-36.8,28.4);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#0F4E6A").s().p("AgiAnQgNgQAAgXQAAgWANgQQAMgRAWAAQAXAAANARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgNgRgAgZgdQgHAMAAARQAAASAHAMQAJANAQAAQAMAAAJgHQAHgIADgMQACgHAAgJQAAgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_88.setTransform(-45.4,28.3);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#0F4E6A").s().p("AAgAzIgHgWIgxAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAHAAADAIIAjBeIABADQgBAGgGAAQgFAAgCgFgAgTARIAnAAIgUg4g");
	this.shape_89.setTransform(-67.6,28.3);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#0F4E6A").s().p("AgcAnQgMgQAAgXQAAgWAMgQQANgRAWAAQASAAALALQADADAAADQAAABAAAAQAAABAAAAQgBABAAAAQAAABgBAAIgEACQgCAAgDgDIgGgEIgGgDIgKgBQgOAAgKAPQgIANAAAPQAAARAIAMQAJAOAPAAQALAAAIgEIAFgDQAEgCACAAQAFAAAAAGQAAADgCACQgFAGgKACQgJADgIAAQgWAAgNgRg");
	this.shape_90.setTransform(-76.9,28.3);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#0F4E6A").s().p("AAUAxIAAgtIgBAAIgnAvQgDAEgEAAQgFAAAAgFQAAgEACgDIAegkQgNAAgJgIQgJgIAAgOQAAgOAJgJQAKgIANAAIAaAAQAGAAAAAHIAABfQAAAHgGAAQgHAAAAgGgAgMglQgGAFAAAJQAAALAGAFQAFAEALAAQAJAAAHgCIAAgmIgSAAQgIAAgGAGg");
	this.shape_91.setTransform(-90,28.4);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#0F4E6A").s().p("AAlA/QgCgBAAgDIAAgPIhAAAQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBAAIAAAEIAAAKQAAADgCABQgCACgCAAQgEAAgCgCQgBgBAAgDIAAgSQgBgGAJgCQAIgDAAgOIAAg/QAAgRASAAIAsAAQAGAAAAAHIAABaIAHAAQAFABAAAEIAAAVQAAADgDABQgCACgCAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBgAgSguIAAA+QAAAKgEAHIAuAAIAAhWIgjAAQgHAAAAAHg");
	this.shape_92.setTransform(-107.7,29.4);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#0F4E6A").s().p("AAeAwIAAhfQAAgIAHAAQAGAAAAAIIAABfQAAAIgGAAQgHAAAAgIgAgkA3QgHAAAAgHIAAhfQABgIAGAAQADAAACADQACACAAADIAAAlIAQAAQAHAAAGACQAIAEAGAHQAGAJgBAKQAAAPgKAJQgJAJgPAAgAgdArIANAAQAKAAAGgGQAGgFAAgKQAAgKgGgGQgGgFgKAAIgNAAg");
	this.shape_93.setTransform(107.8,28.3);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgGAGQgFAFgMAAQgJAAAAgGQABgFAEAAIAJgBQAFgCAAgJIAAhFQAAgRASAAIApAAQAHAAAAAHIAABfQAAADgCACQgCACgEAAQAAAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_94.setTransform(97.4,28.4);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#0F4E6A").s().p("AAgAzIgHgWIgxAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAHAAADAIIAjBeIABADQgBAGgGAAQgFAAgCgFgAgTARIAnAAIgUg4g");
	this.shape_95.setTransform(88.5,28.3);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#0F4E6A").s().p("AgEA1QgCgCAAgDIAAhbIgVAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAgBQABAAAAgBQABAAAAgBQABAAAAAAQABgBAAAAQABAAAAAAQABAAABAAIA2AAQAGAAAAAGQAAAFgGAAIgWAAIAABbQAAADgCACQAAABAAAAQgBABAAAAQgBAAgBAAQAAAAgBAAQgCAAgCgCg");
	this.shape_96.setTransform(26.7,28.4);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#0F4E6A").s().p("AAeAwIAAhfQAAgIAHAAQAGAAAAAIIAABfQAAAIgGAAQgHAAAAgIgAgkA3QgHAAABgHIAAhfQAAgIAGAAQAEAAABADQACACAAADIAAAlIAQAAQAHAAAGACQAIAEAGAHQAGAJAAAKQAAAPgLAJQgJAJgPAAgAgdArIAOAAQAJAAAGgGQAGgFAAgKQAAgKgGgGQgGgFgKAAIgNAAg");
	this.shape_97.setTransform(17.9,28.3);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#0F4E6A").s().p("AgjAnQgMgQAAgXQAAgWAMgQQANgRAWAAQAXAAANARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgOgRgAgZgdQgIAMABARQgBASAIAMQAJANAQAAQAMAAAJgHQAHgIAEgMQABgHAAgJQAAgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_98.setTransform(-2.3,28.3);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#0F4E6A").s().p("AgaA3QgHAAAAgHIAAheQAAgDACgCQAAgBABAAQAAgBAAAAQABAAABgBQAAAAABAAIAvAAQAHAAAAAGQAAAGgHAAIgnAAIAAAgIATAAQAHAAAGACQAKAEAFAHQAGAJAAAKQAAAPgKAJQgKAJgPAAgAgTArIASAAQAJAAAGgGQAGgFAAgKQAAgKgGgGQgHgFgJAAIgRAAg");
	this.shape_99.setTransform(-11.4,28.3);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#0F4E6A").s().p("Ag8AxIAAhgQAAgHAHAAQAHAAAAAHIAAAnIAPAAQACgVAMgMQAMgOATAAQAXAAAMARQAMAPAAAXQAAAWgMAQQgNARgWAAQgUAAgNgQQgMgPAAgWIgPAAIAAAvQAAAHgHAAQgHAAAAgHgAgKgdQgHALAAASQAAARAHAMQAIAOAQAAQANAAAIgIQAIgIADgMIABgPQAAgSgHgLQgJgOgRAAQgQAAgIAOg");
	this.shape_100.setTransform(-22.3,28.4);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#0F4E6A").s().p("AgiAnQgNgQAAgXQAAgWANgQQAMgRAWAAQAXAAANARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgNgRgAgZgdQgHAMAAARQAAASAHAMQAJANAQAAQAMAAAJgHQAHgIADgMQACgHAAgJQAAgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_101.setTransform(-48.1,28.3);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#0F4E6A").s().p("AgWA1QgCgCAAgDIAAhfQAAgDABgCQABAAAAgBQABAAAAgBQABAAAAAAQABAAABAAIAlAAQAGAAAAAGQAAAFgGAAIgeAAIAABbQAAAHgGAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBg");
	this.shape_102.setTransform(-56,28.4);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#0F4E6A").s().p("AAhAzIgIgWIgxAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAHAAACAIIAkBeIAAADQAAAGgGAAQgFAAgBgFgAgTARIAnAAIgUg4g");
	this.shape_103.setTransform(-86.9,28.3);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#0F4E6A").s().p("AAUAxIAAgtIgBAAIgnAvQgDAEgDAAQgHAAABgFQAAgEACgDIAegkQgOAAgIgIQgJgIAAgOQAAgOAJgJQAKgIANAAIAaAAQAHAAgBAHIAABfQABAHgHAAQgHAAAAgGgAgMglQgGAFABAJQgBALAGAFQAFAEAKAAQAKAAAHgCIAAgmIgSAAQgIAAgGAGg");
	this.shape_104.setTransform(-109.2,28.4);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgFAGQgGAFgMAAQgIAAgBgGQAAgFAGAAIAIgBQAFgCAAgJIAAhFQAAgRASAAIAqAAQAFAAAAAHIAABfQAAADgCACQgCACgCAAQgBAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_105.setTransform(-118.1,28.4);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#0F4E6A").s().p("AAlA/QgCgBAAgDIAAgPIhBAAQAAAAgBAAQAAAAgBAAQAAABgBAAQAAAAAAAAIgBAEIAAAKQAAADgCABQgCACgCAAQgDAAgDgCQgCgBAAgDIAAgSQAAgGAJgCQAIgDAAgOIAAg/QAAgRASAAIAsAAQAGAAAAAHIAABaIAHAAQAFABAAAEIAAAVQAAADgCABQgDACgDAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBgAgSguIAAA+QAAAKgEAHIAtAAIAAhWIgjAAQgGAAAAAHg");
	this.shape_106.setTransform(-127,29.4);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#0F4E6A").s().p("AgjAnQgMgQAAgXQAAgWAMgQQANgRAWAAQAWAAAOARQAMAQAAAWQAAAXgNAQQgNARgWAAQgVAAgOgRgAgZgdQgIAMABARQgBASAIAMQAJANAQAAQAMAAAJgHQAIgIADgMQACgHAAgJQgBgRgHgMQgJgNgRAAQgQAAgJANg");
	this.shape_107.setTransform(60.1,28.3);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#0F4E6A").s().p("AAWA2QAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBIAAgmQgLADgJAAQgPAAgJgJQgJgIAAgRIAAgbQAAgIAHAAQAHAAAAAIIAAAbQAAAWATAAQAMAAAIgCIAAgvQAAgIAHAAQAHAAAAAIIAABgQAAADgCACQgBAAAAABQgBAAAAAAQgBABgBAAQAAAAgBAAQgDAAgCgCg");
	this.shape_108.setTransform(15.5,28.3);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#0F4E6A").s().p("AAlA/QgCgBAAgDIAAgPIhBAAQAAAAgBAAQgBAAAAAAQAAABgBAAQAAAAAAAAIgBAEIAAAKQAAADgBABQgDACgCAAQgDAAgDgCQgCgBAAgDIAAgSQABgGAHgCQAJgDAAgOIAAg/QAAgRASAAIAsAAQAGAAAAAHIAABaIAIAAQAEABgBAEIAAAVQAAADgBABQgDACgDAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBgAgSguIAAA+QAAAKgEAHIAtAAIAAhWIgjAAQgGAAAAAHg");
	this.shape_109.setTransform(6.9,29.4);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#0F4E6A").s().p("AAgA0IgggsIgfAsQgCAEgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAgBgBAAIABgFIAjgwIgdgpIgBgFQgBgGAGAAQAEAAADAEIAZAmIAbgmQACgEAEAAQAFAAAAAGIgBAFIgdAqIAjAvIABAFQAAAGgGAAQgEAAgCgEg");
	this.shape_110.setTransform(-12.7,28.3);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#0F4E6A").s().p("AgWA1QgCgCAAgDIAAhfQAAgDACgCQAAAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAIAlAAQAHAAAAAGQAAAFgHAAIgdAAIAABbQABAHgIAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBg");
	this.shape_111.setTransform(-54.1,28.4);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#0F4E6A").s().p("AAgAzIgIgWIgwAAIgIAWQgCAFgEAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQgBAAAAAAQAAgBAAgBQAAAAAAgBIAAgDIAkheQADgIAFAAQAGAAAEAIIAjBeIABADQAAAGgHAAQgFAAgCgFgAgUARIApAAIgVg4g");
	this.shape_112.setTransform(-84.9,28.3);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#0F4E6A").s().p("AAUAxIAAgtIgBAAIgnAvQgDAEgEAAQgFAAAAgFQAAgEADgDIAdgkQgNAAgJgIQgJgIAAgOQAAgOAKgJQAIgIANAAIAbAAQAGAAAAAHIAABfQAAAHgGAAQgHAAAAgGgAgMglQgFAFgBAJQABALAFAFQAGAEAKAAQAJAAAHgCIAAgmIgSAAQgIAAgGAGg");
	this.shape_113.setTransform(-107.3,28.4);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#0F4E6A").s().p("AAfA1QgCgCAAgDIAAhbIggAAQgHAAAAAHIAABEQAAAMgFAGQgGAFgMAAQgJAAABgGQAAgFAEAAIAJgBQAFgCAAgJIAAhFQAAgRASAAIApAAQAHAAgBAHIAABfQAAADgBACQgCACgEAAQAAAAgBAAQgBAAAAAAQgBAAAAgBQgBAAAAgBg");
	this.shape_114.setTransform(-116.2,28.4);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#0F4E6A").s().p("AAlA/QgCgBAAgDIAAgPIhAAAQgBAAgBAAQgBAAAAAAQgBABAAAAQAAAAgBAAIAAAEIAAAKQAAADgCABQgBACgEAAQgDAAgBgCQgCgBAAgDIAAgSQAAgGAIgCQAIgDAAgOIAAg/QAAgRASAAIAsAAQAGAAAAAHIAABaIAHAAQAEABAAAEIAAAVQAAADgCABQgCACgCAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBgAgSguIAAA+QAAAKgEAHIAuAAIAAhWIgjAAQgHAAAAAHg");
	this.shape_115.setTransform(-125,29.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64,p:{x:-132.7}},{t:this.shape_63,p:{x:-123.9}},{t:this.shape_62,p:{x:-115}},{t:this.shape_61,p:{x:-101.9}},{t:this.shape_60,p:{x:-92.6}},{t:this.shape_59,p:{x:-81.8}},{t:this.shape_58,p:{x:-70.4}},{t:this.shape_57,p:{x:-61.8}},{t:this.shape_56},{t:this.shape_55,p:{x:-39.3}},{t:this.shape_54,p:{x:-30.7}},{t:this.shape_53,p:{x:-22.6}},{t:this.shape_52,p:{x:-12.5}},{t:this.shape_51,p:{x:-2.6}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48,p:{x:22.3}},{t:this.shape_47,p:{x:32.6}},{t:this.shape_46,p:{x:42.1}},{t:this.shape_45,p:{x:51.2}},{t:this.shape_44,p:{x:59.9}},{t:this.shape_43},{t:this.shape_42,p:{x:83.4}},{t:this.shape_41},{t:this.shape_40,p:{x:103.1}},{t:this.shape_39,p:{x:113.6}},{t:this.shape_38,p:{x:125.3}},{t:this.shape_37,p:{x:136.6}}]}).to({state:[{t:this.shape_64,p:{x:-140.4}},{t:this.shape_63,p:{x:-131.5}},{t:this.shape_62,p:{x:-122.6}},{t:this.shape_61,p:{x:-109.5}},{t:this.shape_60,p:{x:-100.3}},{t:this.shape_59,p:{x:-89.5}},{t:this.shape_78},{t:this.shape_57,p:{x:-69.4}},{t:this.shape_52,p:{x:-61.5}},{t:this.shape_77,p:{x:-46.7}},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_58,p:{x:-10.6}},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69,p:{x:30.3}},{t:this.shape_68},{t:this.shape_55,p:{x:48.6}},{t:this.shape_45,p:{x:58.9}},{t:this.shape_44,p:{x:67.5}},{t:this.shape_67,p:{x:75.4}},{t:this.shape_42,p:{x:91.1}},{t:this.shape_37,p:{x:101.9}},{t:this.shape_66,p:{x:110.8}},{t:this.shape_39,p:{x:121.2}},{t:this.shape_38,p:{x:133}},{t:this.shape_65,p:{x:144.2}}]},1).to({state:[{t:this.shape_92},{t:this.shape_69,p:{x:-98.9}},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89,p:{x:-67.6}},{t:this.shape_59,p:{x:-56.8}},{t:this.shape_88,p:{x:-45.4}},{t:this.shape_87,p:{x:-36.8}},{t:this.shape_45,p:{x:-28.9}},{t:this.shape_86},{t:this.shape_85},{t:this.shape_51,p:{x:0.3}},{t:this.shape_54,p:{x:8.4}},{t:this.shape_40,p:{x:15.8}},{t:this.shape_84,p:{x:26.3}},{t:this.shape_83},{t:this.shape_77,p:{x:42.8}},{t:this.shape_42,p:{x:58.5}},{t:this.shape_82},{t:this.shape_63,p:{x:78.2}},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79}]},1).to({state:[{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_51,p:{x:-96.1}},{t:this.shape_103},{t:this.shape_59,p:{x:-76.1}},{t:this.shape_52,p:{x:-64.7}},{t:this.shape_102},{t:this.shape_101},{t:this.shape_48,p:{x:-34.4}},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_53,p:{x:7.8}},{t:this.shape_97},{t:this.shape_96},{t:this.shape_55,p:{x:35.2}},{t:this.shape_88,p:{x:45.5}},{t:this.shape_87,p:{x:54.1}},{t:this.shape_45,p:{x:62}},{t:this.shape_42,p:{x:77.7}},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93,p:{x:107.8}},{t:this.shape_38,p:{x:119.6}},{t:this.shape_89,p:{x:130.8}}]},1).to({state:[{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_51,p:{x:-94.2}},{t:this.shape_112},{t:this.shape_59,p:{x:-74.1}},{t:this.shape_67,p:{x:-62.7}},{t:this.shape_111},{t:this.shape_84,p:{x:-46.2}},{t:this.shape_55,p:{x:-31.6}},{t:this.shape_60,p:{x:-21.9}},{t:this.shape_110},{t:this.shape_77,p:{x:-3.1}},{t:this.shape_109},{t:this.shape_108},{t:this.shape_47,p:{x:24.9}},{t:this.shape_46,p:{x:34.4}},{t:this.shape_45,p:{x:43.6}},{t:this.shape_44,p:{x:52.2}},{t:this.shape_107},{t:this.shape_42,p:{x:75.8}},{t:this.shape_65,p:{x:86.6}},{t:this.shape_66,p:{x:95.5}},{t:this.shape_93,p:{x:105.9}},{t:this.shape_38,p:{x:117.7}},{t:this.shape_37,p:{x:128.9}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-175.2,-15.2,354.3,54.4);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00052pngcopy2();
	this.instance.parent = this;
	this.instance.setTransform(-24,-35,0.5,0.5);

	this.instance_1 = new lib.i00052pngcopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-37,-35,0.5,0.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape.setTransform(-48.9,-12.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#013F5D").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_1.setTransform(-57.3,-11.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#013F5D").s().p("AgIAcIAAgzIgNAAQgHgCAAgGQAAgJANAAIAfAAQANAAAAAJQAAAGgIACIgLAAIAAAzQAAANgKAAQgIAAAAgNg");
	this.shape_2.setTransform(-64.2,-11.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#013F5D").s().p("AgKAoQgHAAgDgCQgDgCAAgHIAAg3QAAgHADgDQADgDAHAAIAUAAQANAAAAAJQAAAIgNAAIgOAAIAAAOIAMAAQANAAAAAIQAAAIgNAAIgMAAIAAAPIAPAAQANAAgBAJQABAIgNAAg");
	this.shape_3.setTransform(-69.6,-11.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#013F5D").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_4.setTransform(-76.9,-11.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#013F5D").s().p("AARAhIgEgKIgbAAIgDAKQgCAIgHAAQgEAAgCgCQgCgDgBgDIABgGIAVg3QAFgMAIAAQAKAAAEAKIAUA5IACAGQAAAEgDACQgDACgDAAQgIAAgCgIgAgIAHIARAAIgJgbIAAAAg");
	this.shape_5.setTransform(-84.6,-11.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#013F5D").s().p("AATAlQgBgCAAgHIAAg0IgTAAQgDAAAAAFIAAAsQgBAQgQAAQgPAAAAgIQAAgFADgCIAGgBQAEAAABgBQABgBAAgEIAAgmQAAgMAEgEQAFgFALAAIAcAAQAJAAAAANIAAA3QAAANgJAAQgGAAgCgEg");
	this.shape_6.setTransform(-92.5,-11.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#013F5D").s().p("AARAcIAAgzIghAAIAAAzQAAANgJAAQgJAAAAgNIAAg3QAAgNAMAAIAtAAQAMAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_7.setTransform(-100.8,-11.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#013F5D").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape_8.setTransform(-48.9,-28);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#013F5D").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_9.setTransform(-57.3,-26.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#013F5D").s().p("AgWAcIAAg3QAAgHADgDQACgDAIAAIATAAQANAAAAAJQAAAIgNAAIgOAAIAAAzQAAANgJAAQgJAAAAgNg");
	this.shape_10.setTransform(-63.9,-26.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#013F5D").s().p("AgOAoIABgFIAFgNIgXg3IgBgGQAAgEACgCQADgDAEAAQAFAAAEAIIAOAnIABAAIAOgmIADgGQACgDAEAAQAIAAAAAJIgBAGIgdBKIgCAGQgCACgEAAQgIAAAAgJg");
	this.shape_11.setTransform(-70.1,-26.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#013F5D").s().p("AgcAcIAAg3QAAgHADgDQACgDAIAAIAPAAQANAAAIAHQAIAIAAANQAAAMgHAHQgIAHgMAAIgMAAIAAAOQAAANgJAAQgJAAAAgNgAgKgCIAIAAQAMAAAAgKQAAgLgMAAIgIAAg");
	this.shape_12.setTransform(-76.6,-26.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#013F5D").s().p("AAaAuQgDgDAAgDIAAgIIgtAAIAAAIQAAADgCADQgCACgFAAQgDAAgCgCQgDgDAAgDIAAgPQABgIAGAAQABAAABAAQAAAAABgBQAAAAAAgBQABAAAAgBIABgMIAAgdQAAgJAFgFQAGgGAJAAIAbAAQAGAAADAEQABADAAAGIAAAzQAJAAAAAIIAAAPQABAIgJAAQgDAAgCgCgAgLgaIAAAcQAAAIgCAHIAZAAIAAgwIgSAAQgEABgBAEg");
	this.shape_13.setTransform(-84.4,-26.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#013F5D").s().p("AgVAeQgKgLAAgTQAAgRALgMQAKgMARAAQAHAAAFACQAIACADAEIABAFQAAADgCACQgCADgDAAIgIgCQgEgCgFAAQgIAAgGAIQgEAHAAAKQAAAKAFAGQAFAIAJAAQAEAAAGgCIAHgCQADAAACACQACADAAADIgBAFQgHAIgRAAQgRAAgLgMg");
	this.shape_14.setTransform(-95.7,-26.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#013F5D").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_15.setTransform(-107.2,-26.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#013F5D").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_16.setTransform(-115.6,-26.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#013F5D").s().p("AgHAlIgBgKQgMAAgGgCQgIgDgFgHQgFgHAAgIQAAgJAFgGQAFgHAIgDQAFgCANAAIABgJQACgEAFAAQAGAAACAEIABAJQANAAAFACQAIADAFAHQAFAGAAAJQAAAIgFAHQgFAHgIADQgGACgMAAIgBAKQgCAEgGAAQgFAAgCgEgAAJAMIAFAAQAMAAAAgMQAAgNgMAAIgFAAgAgZAAQAAAMAMAAIAFAAIAAgZIgFAAQgMAAAAANg");
	this.shape_17.setTransform(-124.5,-26.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-135.3,-35.4,127.8,32.4), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00052pngcopy();
	this.instance.parent = this;
	this.instance.setTransform(29.5,-35,0.5,0.5,0,0,180);

	this.instance_1 = new lib.i00052pngcopy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(42.5,-35,0.5,0.5,0,0,180);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape.setTransform(107.6,-12.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#013F5D").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_1.setTransform(99.2,-11.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#013F5D").s().p("AgIAcIAAgzIgMAAQgIgCAAgGQAAgJANAAIAfAAQANAAAAAJQAAAGgIACIgMAAIAAAzQAAANgJAAQgIAAAAgNg");
	this.shape_2.setTransform(92.3,-11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#013F5D").s().p("AgKAoQgHAAgCgCQgDgCgBgHIAAg3QABgHADgDQACgDAIAAIATAAQANAAAAAJQAAAIgNAAIgOAAIAAAOIAMAAQAMAAAAAIQAAAIgMAAIgMAAIAAAPIAOAAQAOAAAAAJQAAAIgOAAg");
	this.shape_3.setTransform(86.9,-11.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#013F5D").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_4.setTransform(79.6,-11.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#013F5D").s().p("AAQAhIgCgKIgbAAIgEAKQgDAIgGAAQgEAAgCgCQgDgDAAgDIACgGIAVg3QAEgMAIAAQAKAAAEAKIAUA5IABAGQAAAEgCACQgDACgEAAQgHAAgDgIgAgJAHIARAAIgIgbIAAAAg");
	this.shape_5.setTransform(71.9,-11.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#013F5D").s().p("AAUAlQgCgCAAgHIAAg0IgTAAQgEAAABAFIAAAsQAAAQgRAAQgOAAAAgIQAAgFADgCIAFgBQAEAAABgBQABgBAAgEIAAgmQAAgMAFgEQAEgFALAAIAcAAQAKAAAAANIAAA3QAAANgKAAQgFAAgCgEg");
	this.shape_6.setTransform(64,-11.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#013F5D").s().p("AARAcIAAgzIghAAIAAAzQAAANgJAAQgJAAAAgNIAAg3QAAgNAMAAIAtAAQAMAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_7.setTransform(55.7,-11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#013F5D").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape_8.setTransform(131.8,-27.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#013F5D").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_9.setTransform(123.4,-26.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#013F5D").s().p("AgWAcIAAg3QAAgHADgDQACgDAIAAIATAAQANAAAAAJQAAAIgNAAIgOAAIAAAzQAAANgJAAQgJAAAAgNg");
	this.shape_10.setTransform(116.8,-26.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#013F5D").s().p("AgOAoIABgFIAFgNIgWg3IgBgGQAAgEABgCQADgDAEAAQAFAAAEAIIAOAnIAAAAIAPgmIADgGQACgDAEAAQAIAAAAAJIgBAGIgdBKIgCAGQgDACgDAAQgIAAAAgJg");
	this.shape_11.setTransform(110.6,-25.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#013F5D").s().p("AgcAcIAAg3QAAgHADgDQACgDAIAAIAPAAQANAAAIAHQAIAIAAANQAAAMgHAHQgIAHgMAAIgMAAIAAAOQAAANgJAAQgJAAAAgNgAgKgCIAIAAQAMAAAAgKQAAgLgMAAIgIAAg");
	this.shape_12.setTransform(104.1,-26.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#013F5D").s().p("AAZAtQgCgBAAgFIAAgGIgtAAIAAAGQAAAFgCABQgCADgEAAQgEAAgDgDQgCgBAAgFIAAgOQAAgIAIAAQAAAAABAAQAAAAABgBQAAAAAAgBQABAAAAgBIABgMIAAgcQAAgKAFgGQAFgFAKAAIAbAAQAGAAADAEQABADAAAGIAAAzQAKAAAAAIIAAAOQAAAJgJAAQgDAAgDgDgAgLgaIAAAcQABAIgDAHIAZAAIAAgwIgSAAQgFAAAAAFg");
	this.shape_13.setTransform(96.3,-25.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#013F5D").s().p("AgVAeQgKgLAAgTQAAgRALgMQAKgMARAAQAHAAAFACQAIACADAEIABAFQAAADgCACQgCADgDAAIgIgCQgEgCgFAAQgIAAgGAIQgEAHAAAKQAAAKAFAGQAFAIAJAAQAEAAAGgCIAHgCQADAAACACQACADAAADIgBAFQgHAIgRAAQgRAAgLgMg");
	this.shape_14.setTransform(85,-26.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#013F5D").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_15.setTransform(73.5,-26.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#013F5D").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_16.setTransform(65.1,-26.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#013F5D").s().p("AgHAlIgBgKQgMAAgGgCQgIgDgFgHQgFgHAAgIQAAgJAFgGQAFgHAIgDQAFgCANAAIABgJQACgEAFAAQAGAAACAEIABAJQANAAAFACQAIADAFAHQAFAGAAAJQAAAIgFAHQgFAHgIADQgGACgMAAIgBAKQgCAEgGAAQgFAAgCgEgAAJAMIAFAAQAMAAAAgMQAAgNgMAAIgFAAgAgZAAQAAAMAMAAIAFAAIAAgZIgFAAQgMAAAAANg");
	this.shape_17.setTransform(56.2,-26.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(13,-35,130.6,32.4), null);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00053pngcopy2();
	this.instance.parent = this;
	this.instance.setTransform(-8.5,-35,0.5,0.5,0,0,180);

	this.instance_1 = new lib.i00053pngcopy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-21.5,-35,0.5,0.5,0,0,180);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape.setTransform(-48.9,-12.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_1.setTransform(-57.3,-11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIAcIAAgzIgNAAQgHgCAAgGQAAgJANAAIAfAAQANAAAAAJQAAAGgIACIgLAAIAAAzQAAANgKAAQgIAAAAgNg");
	this.shape_2.setTransform(-64.2,-11.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgKAoQgHAAgDgCQgDgCAAgHIAAg3QAAgHADgDQADgDAHAAIAUAAQANAAAAAJQAAAIgNAAIgOAAIAAAOIAMAAQANAAAAAIQAAAIgNAAIgMAAIAAAPIAPAAQANAAgBAJQABAIgNAAg");
	this.shape_3.setTransform(-69.6,-11.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_4.setTransform(-76.9,-11.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AARAhIgEgKIgbAAIgDAKQgCAIgHAAQgEAAgCgCQgCgDgBgDIABgGIAVg3QAFgMAIAAQAKAAAEAKIAUA5IACAGQAAAEgDACQgDACgDAAQgIAAgCgIgAgIAHIARAAIgJgbIAAAAg");
	this.shape_5.setTransform(-84.6,-11.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AATAlQgBgCAAgHIAAg0IgTAAQgDAAAAAFIAAAsQgBAQgQAAQgPAAAAgIQAAgFADgCIAGgBQAEAAABgBQABgBAAgEIAAgmQAAgMAEgEQAFgFALAAIAcAAQAJAAAAANIAAA3QAAANgJAAQgGAAgCgEg");
	this.shape_6.setTransform(-92.5,-11.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AARAcIAAgzIghAAIAAAzQAAANgJAAQgJAAAAgNIAAg3QAAgNAMAAIAtAAQAMAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_7.setTransform(-100.8,-11.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape_8.setTransform(-48.9,-27.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_9.setTransform(-57.3,-26.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgWAcIAAg3QAAgHADgDQACgDAIAAIATAAQANAAAAAJQAAAIgNAAIgOAAIAAAzQAAANgJAAQgJAAAAgNg");
	this.shape_10.setTransform(-63.9,-26.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgOAoIABgFIAFgNIgXg3IgBgGQAAgEACgCQADgDAEAAQAFAAAEAIIAOAnIABAAIAOgmIADgGQACgDAEAAQAIAAAAAJIgBAGIgdBKIgCAGQgCACgEAAQgIAAAAgJg");
	this.shape_11.setTransform(-70.1,-26);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgcAcIAAg3QAAgHADgDQACgDAIAAIAPAAQANAAAIAHQAIAIAAANQAAAMgHAHQgIAHgMAAIgMAAIAAAOQAAANgJAAQgJAAAAgNgAgKgCIAIAAQAMAAAAgKQAAgLgMAAIgIAAg");
	this.shape_12.setTransform(-76.6,-26.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAaAuQgDgCAAgFIAAgHIgtAAIAAAHQAAAFgCACQgCACgFAAQgDAAgCgCQgDgCAAgFIAAgOQABgIAGAAQABAAABAAQAAAAABgBQAAAAAAgBQABAAAAgBIABgMIAAgcQAAgKAFgFQAGgGAJAAIAbAAQAGAAADAEQABADAAAGIAAAzQAJAAAAAIIAAAOQABAJgJAAQgDAAgCgCgAgLgaIAAAcQAAAJgCAGIAZAAIAAgwIgSAAQgEABgBAEg");
	this.shape_13.setTransform(-84.4,-26);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgVAeQgKgLAAgTQAAgRALgMQAKgMARAAQAHAAAFACQAIACADAEIABAFQAAADgCACQgCADgDAAIgIgCQgEgCgFAAQgIAAgGAIQgEAHAAAKQAAAKAFAGQAFAIAJAAQAEAAAGgCIAHgCQADAAACACQACADAAADIgBAFQgHAIgRAAQgRAAgLgMg");
	this.shape_14.setTransform(-95.7,-26.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_15.setTransform(-107.2,-26.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_16.setTransform(-115.6,-26.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgHAlIgBgKQgMAAgGgCQgIgDgFgHQgFgHAAgIQAAgJAFgGQAFgHAIgDQAFgCANAAIABgJQACgEAFAAQAGAAACAEIABAJQANAAAFACQAIADAFAHQAFAGAAAJQAAAIgFAHQgFAHgIADQgGACgMAAIgBAKQgCAEgGAAQgFAAgCgEgAAJAMIAFAAQAMAAAAgMQAAgNgMAAIgFAAgAgZAAQAAAMAMAAIAFAAIAAgZIgFAAQgMAAAAANg");
	this.shape_17.setTransform(-124.5,-26.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(-135.3,-35.3,126.8,32.4), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.i00053pngcopy();
	this.instance.parent = this;
	this.instance.setTransform(13,-35,0.5,0.5);

	this.instance_1 = new lib.i00053pngcopy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(27,-35,0.5,0.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape.setTransform(107.5,-12.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_1.setTransform(99.1,-11.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIAcIAAgzIgMAAQgIgCAAgGQAAgJANAAIAfAAQANAAAAAJQAAAGgIACIgLAAIAAAzQgBANgJAAQgIAAAAgNg");
	this.shape_2.setTransform(92.2,-11.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgKAoQgHAAgCgCQgEgCAAgHIAAg3QAAgHAEgDQACgDAIAAIATAAQANAAAAAJQAAAIgNAAIgOAAIAAAOIAMAAQAMAAAAAIQAAAIgMAAIgMAAIAAAPIAOAAQAOAAAAAJQAAAIgOAAg");
	this.shape_3.setTransform(86.8,-11.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_4.setTransform(79.5,-11.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAQAhIgCgKIgbAAIgEAKQgDAIgGAAQgDAAgDgCQgDgDAAgDIABgGIAWg3QAEgMAIAAQAKAAAEAKIAUA5IABAGQAAAEgCACQgDACgEAAQgGAAgEgIgAgJAHIARAAIgIgbIAAAAg");
	this.shape_5.setTransform(71.8,-11.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAUAlQgCgCAAgHIAAg0IgTAAQgDAAAAAFIAAAsQAAAQgRAAQgPAAAAgIQABgFADgCIAFgBQAEAAABgBQABgBAAgEIAAgmQAAgMAFgEQADgFAMAAIAcAAQAKAAAAANIAAA3QAAANgKAAQgFAAgCgEg");
	this.shape_6.setTransform(63.9,-11.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AARAcIAAgzIghAAIAAAzQAAANgJAAQgJAAAAgNIAAg3QAAgNAMAAIAtAAQAMAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_7.setTransform(55.6,-11.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AARAoIAAglIggAsQgFAGgGAAQgIAAAAgLIAAg6QAAgNAJAAQAJAAAAANIAAAlIAhgsQAEgGAGAAQAIAAAAANIAAA4QAAANgJAAQgJAAAAgNgAgMglQgGgEAAgFQAAgGAGAAQAEAAACAEQACAEAEAAQAFAAACgEQACgEAEAAQAGAAAAAGQAAAHgHADQgFAEgHAAQgHAAgFgFg");
	this.shape_8.setTransform(131.7,-27.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_9.setTransform(123.3,-26.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgWAcIAAg3QAAgHADgDQACgDAIAAIATAAQANAAAAAJQAAAIgNAAIgOAAIAAAzQAAANgJAAQgJAAAAgNg");
	this.shape_10.setTransform(116.7,-26.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgOAoIABgFIAFgNIgWg3IgBgGQAAgEACgCQACgDAEAAQAFAAAEAIIAOAnIAAAAIAPgmIADgGQACgDAEAAQAJAAAAAJIgCAGIgdBKIgCAGQgDACgDAAQgIAAAAgJg");
	this.shape_11.setTransform(110.5,-25.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgcAcIAAg3QAAgHADgDQACgDAIAAIAPAAQANAAAIAHQAIAIAAANQAAAMgHAHQgIAHgMAAIgMAAIAAAOQAAANgJAAQgJAAAAgNgAgKgCIAIAAQAMAAAAgKQAAgLgMAAIgIAAg");
	this.shape_12.setTransform(104,-26.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAZAuQgCgDAAgEIAAgHIgtAAIAAAHQAAAEgCADQgDACgDAAQgEAAgDgCQgCgDAAgEIAAgOQAAgIAIAAQAAAAABAAQAAAAABgBQAAAAAAgBQABAAAAgBIABgMIAAgcQAAgKAGgGQAEgFAKAAIAbAAQAGAAADAEQABADAAAGIAAAzQAKAAAAAIIAAAOQgBAJgHAAQgEAAgDgCgAgLgaIAAAcQABAJgDAGIAZAAIAAgwIgSAAQgFABAAAEg");
	this.shape_13.setTransform(96.2,-25.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgVAeQgKgLAAgTQAAgRALgMQAKgMARAAQAHAAAFACQAIACADAEIABAFQAAADgCACQgCADgDAAIgIgCQgEgCgFAAQgIAAgGAIQgEAHAAAKQAAAKAFAGQAFAIAJAAQAEAAAGgCIAHgCQADAAACACQACADAAADIgBAFQgHAIgRAAQgRAAgLgMg");
	this.shape_14.setTransform(84.9,-26.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AARAcIAAgUIghAAIAAAUQAAANgJAAQgJAAAAgNIAAg3QAAgNAJAAQAJAAAAANIAAATIAhAAIAAgTQAAgNAJAAQAJAAAAANIAAA3QAAANgJAAQgJAAAAgNg");
	this.shape_15.setTransform(73.4,-26.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgbAdQgKgLAAgSQAAgRAKgLQAKgMARAAQASAAAKAMQAKALAAARQAAASgKAMQgKALgSAAQgRAAgKgMgAgOgPQgEAGAAAJQAAAKAEAHQAFAHAJAAQAJAAAGgHQAEgHAAgKQAAgJgEgGQgFgIgKAAQgJAAgFAIg");
	this.shape_16.setTransform(65,-26.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgHAlIgBgKQgMAAgGgCQgIgDgFgHQgFgHAAgIQAAgJAFgGQAFgHAIgDQAFgCANAAIABgJQACgEAFAAQAGAAACAEIABAJQANAAAFACQAIADAFAHQAFAGAAAJQAAAIgFAHQgFAHgIADQgGACgMAAIgBAKQgCAEgGAAQgFAAgCgEgAAJAMIAFAAQAMAAAAgMQAAgNgMAAIgFAAgAgZAAQAAAMAMAAIAFAAIAAgZIgFAAQgMAAAAANg");
	this.shape_17.setTransform(56.1,-26.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(13,-35.2,129,32.4), null);


(lib.deletemc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FC3B30").s().p("AgnBaQgIAAgGgGQgGgFAAgJIAAh3IB3AAIAAB3QAAAJgGAFQgGAGgIAAgAgnBGIBPAAIAAhjIhPAAgAAAAjIgVAUIgNgOIAUgVIgUgUIANgOIAVAUIAVgUIAOAOIgVAUIAVAVIgOAOgAhFg7IAAgUIAjAAIAKgKIAxAAIAKAKIAjAAIAAAUg");
	this.shape.setTransform(0,0,2.221,2.222);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.deletemc, new cjs.Rectangle(-15.5,-20,31.1,40), null);


(lib.Symbol31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.fon = new lib.Symbol30();
	this.fon.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.fon).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol31, new cjs.Rectangle(0,0,100,100), null);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 6
	this.exit = new lib.Symbol32();
	this.exit.parent = this;
	this.exit.setTransform(202,-250);

	this.timeline.addTween(cjs.Tween.get(this.exit).wait(1));

	// Layer 5
	this.imgHolder = new lib.Symbol31();
	this.imgHolder.parent = this;
	this.imgHolder.setTransform(-137,-8,1,1,0,0,0,50,50);

	this.timeline.addTween(cjs.Tween.get(this.imgHolder).wait(1));

	// Layer 4
	this.fb = new lib.Symbol29();
	this.fb.parent = this;
	this.fb.setTransform(54,-100);

	this.ok = new lib.Symbol28();
	this.ok.parent = this;
	this.ok.setTransform(0,-100);

	this.vk = new lib.Symbol27();
	this.vk.parent = this;
	this.vk.setTransform(-54,-100);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.vk},{t:this.ok},{t:this.fb}]}).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#013F5D").s().p("AgHAvQgEgEAAgFQAAgFAEgDQADgDAEAAQAFAAADADQAEADAAAFQAAAFgEAEQgDADgFAAQgEAAgDgDgAgJAEIAAgmQAAgPAJAAQAKAAAAAPIAAAmQAAAPgKAAQgJAAAAgPg");
	this.shape.setTransform(38.6,-146.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#013F5D").s().p("AATApIgTgbIgSAbQgGAHgFABQgEAAgDgDQgEgDAAgDQABgFAFgHIAXgeIgRgZQgGgHAAgDQAAgFADgDQADgDAFABQAGAAAFAIIAMAUIAOgUQAFgJAHABQAEAAACACQADADAAAEQAAAEgFAHIgSAZIAYAeQAFAHgBAFQAAADgDADQgCADgEAAQgHAAgFgIg");
	this.shape_1.setTransform(32.8,-146.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#013F5D").s().p("AANAhIAAgXIgYAfQgGAHgGAAQgEAAgDgCQgDgDAAgFQAAgEAGgHIARgTQgKgDgGgFQgGgHAAgKQAAgNAJgJQAJgIARAAIARAAQAIAAADAEQADACAAAJIAABBQAAAPgKAAQgLAAAAgPgAgKgPQAAALANAAIAKAAIAAgXIgKAAQgNAAAAAMg");
	this.shape_2.setTransform(25.2,-146.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#013F5D").s().p("AgJAhIAAg8IgPAAQgJgDAAgGQAAgLAQAAIAkAAQAPAAAAALQAAAGgIADIgPAAIAAA8QAAAPgLAAQgKAAABgPg");
	this.shape_3.setTransform(17.8,-146.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#013F5D").s().p("AgMAvQgIAAgDgDQgDgCAAgIIAAhBQAAgJADgDQADgDAIAAIAYAAQAPAAAAAKQAAAJgPAAIgRAAIAAARIAPAAQAOAAAAAKQAAAKgOgBIgPAAIAAASIARAAQAQABAAAJQAAAKgQAAg");
	this.shape_4.setTransform(11.4,-146.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#013F5D").s().p("AgZAjQgMgNAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAFQACADAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAJQgGAJAAAKQAAANAGAHQAGAJALAAQAGAAAGgDIAIgBQAEAAADACQACADAAAEQAAADgCADQgHAKgVAAQgUAAgNgPg");
	this.shape_5.setTransform(3.7,-146.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#013F5D").s().p("AAdA3QgCgDAAgEIAAgJIg6AAQgOAAAAgQIAAhAQAAgPALAAQALAAgBAPIAAA+IAlAAIAAg+QAAgPALAAQALAAAAAPIAAA+QALAAAAAJIAAASQAAAJgJAAQgFAAgDgCg");
	this.shape_6.setTransform(-5.2,-145.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#013F5D").s().p("AggAiQgMgNAAgVQAAgUAMgOQAMgOAUABQAVgBAMAOQANAOAAAUQAAAVgNAOQgMAOgVAAQgUAAgMgPgAgQgTQgGAJAAAKQAAAMAGAIQAFAJALAAQALAAAGgJQAFgIAAgMQAAgKgEgJQgHgJgLAAQgLAAgFAJg");
	this.shape_7.setTransform(-15.6,-146.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#013F5D").s().p("AgZAjQgMgNAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAFQACADAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAJQgGAJAAAKQAAANAGAHQAGAJALAAQAGAAAGgDIAIgBQAEAAADACQACADAAAEQAAADgCADQgHAKgVAAQgUAAgNgPg");
	this.shape_8.setTransform(-24.7,-146.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#013F5D").s().p("AgUAvQgIAAgDgDQgDgCAAgIIAAhBQAAgJADgDQADgDAJAAIARAAQANAAAIAFQAMAGAAANQAAAPgMAFIAAABQAJABADAFQAEAFAAAJQAAAPgLAHQgIAHgPgBgAgMAdIAKAAQAGAAADgCQAFgDAAgGQAAgHgFgDQgCgBgHAAIgKAAgAgMgKIAJAAQAGAAACgBQAEgCgBgGQAAgJgLAAIgJAAg");
	this.shape_9.setTransform(-37.1,-146.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#013F5D").s().p("AAUAvIAAgsIgnA0QgFAHgHAAQgKAAABgMIAAhEQgBgQALAAQALAAAAAQIAAArIAng0QAFgHAHAAQAJAAABAPIAABCQgBAPgKAAQgLAAAAgPgAgPgrQgGgFAAgGQAAgHAHAAQADAAADAEQADAEAFAAQAFAAADgEQADgEAEAAQAHAAAAAHQAAAHgIAFQgGAEgIAAQgJAAgGgFg");
	this.shape_10.setTransform(131.1,-163.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#013F5D").s().p("AghAjQgLgNgBgWQABgUALgOQAMgOAVAAQAVAAANAOQAMAOgBAUQABAWgMANQgNANgVAAQgVABgMgOgAgRgSQgFAHAAAMQAAALAFAIQAHAJAKAAQALAAAGgJQAGgIAAgLQgBgMgFgHQgGgJgLAAQgKAAgHAJg");
	this.shape_11.setTransform(121.2,-162.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#013F5D").s().p("AAQApIgbghIgEADIAAAXQAAAPgLgBQgLABAAgPIAAhCQAAgPALgBQALABAAAPIAAAUIAagcQAIgIAEAAQAEABAEADQADACAAAFQAAADgDADIgGAGIgWAVIAdAgQAGAGAAAEQAAAEgDAEQgDACgEAAQgFAAgHgHg");
	this.shape_12.setTransform(112.6,-162.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#013F5D").s().p("AgJAhIAAg8IgPgBQgJgBAAgIQAAgKAQAAIAkAAQAPAAAAAKQAAAIgIABIgPABIAAA8QAAAPgLAAQgJAAAAgPg");
	this.shape_13.setTransform(104.7,-162.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#013F5D").s().p("AAcAiIAAhCQAAgPALgBQALABAAAPIAABCQAAAPgLgBQgLABAAgPgAgiAvQgIAAgEgDQgDgDAAgJIAAhAQAAgPALgBQALABAAAPIAAAQIALAAQAQAAAHAJQAJAHAAAQQAAAPgKAIQgIAJgQgBgAgbAcIAHAAQAOgBAAgLQAAgNgOgBIgHAAg");
	this.shape_14.setTransform(95.6,-162.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#013F5D").s().p("AgiAhIAAhAQAAgJAEgEQADgDAJAAIASAAQAQAAAJAIQAKAJAAAQQAAAOgJAIQgJAIgQAAIgNAAIAAARQAAAPgLAAQgLAAAAgPgAgMgCIAKAAQANAAAAgMQAAgNgNAAIgKAAg");
	this.shape_15.setTransform(86,-162.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#013F5D").s().p("AAQApIgbghIgEADIAAAXQAAAPgLgBQgLABAAgPIAAhCQAAgPALgBQALABAAAPIAAAUIAagcQAIgIAEAAQAEABAEADQADACAAAFQAAADgDADIgGAGIgWAVIAdAgQAGAGAAAEQAAAEgDAEQgDACgEAAQgFAAgHgHg");
	this.shape_16.setTransform(78,-162.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#013F5D").s().p("AgJAhIAAg8IgQgBQgIgBAAgIQAAgKAQAAIAkAAQAPAAAAAKQAAAIgIABIgPABIAAA8QAAAPgLAAQgJAAAAgPg");
	this.shape_17.setTransform(70.2,-162.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#013F5D").s().p("AggAjQgNgNAAgWQAAgUANgOQAMgOAUAAQAVAAAMAOQAMAOAAAUQAAAWgMANQgMANgVAAQgUABgMgOgAgRgSQgFAHAAAMQAAALAFAIQAHAJAKAAQALAAAGgJQAFgIABgLQAAgMgGgHQgGgJgLAAQgKAAgHAJg");
	this.shape_18.setTransform(62,-162.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#013F5D").s().p("AAUAvIAAgsIgmA0QgGAHgHAAQgKAAAAgMIAAhEQAAgQALAAQALAAAAAQIAAArIAng0QAFgHAGAAQALAAAAAPIAABCQAAAPgMAAQgKAAAAgPgAgPgrQgGgFAAgGQAAgHAHAAQAEAAACAEQADAEAFAAQAGAAACgEQADgEAEAAQAHAAAAAHQAAAHgIAFQgGAEgIAAQgJAAgGgFg");
	this.shape_19.setTransform(47.7,-163.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#013F5D").s().p("AghAjQgMgNABgWQgBgUAMgOQANgOAUAAQAVAAANAOQAMAOgBAUQABAWgMANQgNANgVAAQgUABgNgOgAgRgSQgFAHAAAMQAAALAFAIQAHAJAKAAQALAAAGgJQAGgIgBgLQAAgMgEgHQgHgJgLAAQgKAAgHAJg");
	this.shape_20.setTransform(37.8,-162.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#013F5D").s().p("AAQApIgbghIgEADIAAAXQAAAPgLgBQgLABAAgPIAAhCQAAgPALgBQALABAAAPIAAAUIAagcQAIgIAEAAQAEABAEADQADACAAAFQAAADgDADIgGAGIgWAVIAdAgQAGAGAAAEQAAAEgDAEQgDACgEAAQgFAAgHgHg");
	this.shape_21.setTransform(29.1,-162.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#013F5D").s().p("AgZAkQgMgOAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAEQACAEAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAKQgGAHAAAMQAAAMAGAHQAGAJALAAQAGAAAGgCIAIgCQAEAAADADQACACAAAEQAAADgCADQgHAKgVAAQgUAAgNgOg");
	this.shape_22.setTransform(20.4,-162.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#013F5D").s().p("AgMAvQgIAAgEgCQgDgDAAgIIAAhBQAAgIADgEQAEgDAJAAIAWAAQAQgBAAALQAAAKgQAAIgQAAIAAARIAOAAQAPAAABAJQgBAKgPAAIgOAAIAAASIARAAQAQgBgBAKQABALgQgBg");
	this.shape_23.setTransform(13.1,-162.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#013F5D").s().p("AAMAiIAAgSIgOAAQgPgBgIgHQgIgIAAgQIAAgRQAAgPALAAQALAAAAAPIAAARQAAAIADACQACADAHAAIALAAIAAgeQAAgPALAAQALAAAAAPIAABDQAAAPgLgBQgLABAAgPg");
	this.shape_24.setTransform(5.3,-162.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#013F5D").s().p("AAUAiIAAgrIgnAyQgFAIgHgBQgKAAAAgMIAAhEQAAgPALgBQALABAAAPIAAArIAng0QAGgGAFgBQALAAgBAPIAABDQABAPgMgBQgKABAAgPg");
	this.shape_25.setTransform(-3.5,-162.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#013F5D").s().p("AAmAtQgCgDgBgHIgIgzIAAAAIgMAvQgCAHgDADQgDAFgHAAQgGAAgDgFQgDgDgCgHIgMgvIAAAAIgIAzQgBAHgCADQgDADgGAAQgJAAAAgKIABgHIAKhAQABgGAFgEQAEgEAHgBQAGAAAEAEQAEAEABAFIAMA1IAAAAIANg1QABgFAEgEQAFgEAFAAQAHABAEAEQAFAEABAGIALBAIAAAHQAAAKgJAAQgGAAgDgDg");
	this.shape_26.setTransform(-14.3,-162.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#013F5D").s().p("AgZAkQgMgOAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAEQACAEAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAKQgGAHAAAMQAAAMAGAHQAGAJALAAQAGAAAGgCIAIgCQAEAAADADQACACAAAEQAAADgCADQgHAKgVAAQgUAAgNgOg");
	this.shape_27.setTransform(-24.2,-162.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#013F5D").s().p("AggAjQgNgNAAgWQAAgUANgOQALgOAVAAQAVAAAMAOQANAOAAAUQAAAWgNANQgMANgVAAQgVABgLgOgAgQgSQgGAHAAAMQAAALAGAIQAFAJALAAQALAAAGgJQAFgIAAgLQABgMgGgHQgGgJgLAAQgLAAgFAJg");
	this.shape_28.setTransform(-33.3,-162.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#013F5D").s().p("AAQApIgbghIgEADIAAAXQAAAPgLgBQgLABAAgPIAAhCQAAgPALgBQALABAAAPIAAAUIAagcQAIgIAEAAQAEABAEADQADACAAAFQAAADgDADIgGAGIgWAVIAdAgQAGAGAAAEQAAAEgDAEQgDACgEAAQgFAAgHgHg");
	this.shape_29.setTransform(-41.9,-162.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#013F5D").s().p("AgTAwQgIAAgDgEQgEgCAAgJIAAhAQAAgQALAAQALAAAAAQIAAAPIANAAQAQAAAJAJQAIAHAAAQQAAAPgJAJQgJAIgQAAgAgMAcIAKAAQANAAAAgMQAAgNgNAAIgKAAg");
	this.shape_30.setTransform(-54.7,-162.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#013F5D").s().p("AgZAkQgMgOAAgWQAAgUANgOQAMgPAVAAQAHAAAHACQAIADAEAEQACAEAAACQAAAEgDADQgCADgEAAQgEAAgFgCQgFgCgFAAQgLAAgGAKQgGAHAAAMQAAAMAGAHQAGAJALAAQAGAAAGgCIAIgCQAEAAADADQACACAAAEQAAADgCADQgHAKgVAAQgUAAgNgOg");
	this.shape_31.setTransform(-63.1,-162.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#013F5D").s().p("AgMAvQgIAAgEgCQgCgDAAgIIAAhBQAAgIACgEQAEgDAIAAIAXAAQAQgBAAALQAAAKgQAAIgQAAIAAARIAPAAQAOAAAAAJQAAAKgOAAIgPAAIAAASIARAAQAPgBABAKQgBALgPgBg");
	this.shape_32.setTransform(-70.4,-162.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#013F5D").s().p("AgKAhIAAg8IgOgBQgJgBAAgIQAAgKAQAAIAkAAQAPAAAAAKQAAAIgJABIgOABIAAA8QAAAPgLAAQgKAAAAgPg");
	this.shape_33.setTransform(-77.3,-162.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#013F5D").s().p("AAUAiIAAgrIgmAyQgGAIgHgBQgKAAAAgMIAAhEQAAgPALgBQALABAAAPIAAArIAng0QAFgGAGgBQALAAAAAPIAABDQAAAPgMgBQgKABAAgPg");
	this.shape_34.setTransform(-85.5,-162.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#013F5D").s().p("AAXAsQgCgDAAgHIAAg+IgWAAQgFAAAAAFIAAA0QAAAUgTgBQgRABAAgLQAAgFADgDIAHgBQAFABABgCIABgGIAAgtQAAgOAFgFQAFgGAOAAIAhAAQALABAAAPIAABCQAAAPgLgBQgHABgCgFg");
	this.shape_35.setTransform(-95.7,-162.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#013F5D").s().p("AgMAvQgIAAgDgCQgEgDAAgIIAAhBQAAgIAEgEQADgDAIAAIAYAAQAPgBAAALQAAAKgPAAIgRAAIAAARIAOAAQAQAAgBAJQABAKgQAAIgOAAIAAASIASAAQAOgBAAAKQAAALgOgBg");
	this.shape_36.setTransform(-103.6,-162.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#013F5D").s().p("AAeA2QgCgDgBgEIAAgJIg1AAIAAAJQgBAEgCADQgDADgEgBQgFABgDgDQgCgDAAgEIAAgSQAAgJAJAAQACAAABgDQACgFAAgKIAAgiQAAgLAGgGQAHgHALABIAgAAQAGgBAEAFQACADAAAIIAAA8QALAAAAAJIAAASQAAAJgKAAQgEABgDgDgAgNgfIAAAhQAAALgDAHIAeAAIAAg4IgWAAQgFgBAAAGg");
	this.shape_37.setTransform(-112.1,-161.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#013F5D").s().p("AghAjQgLgNgBgWQABgUALgOQAMgOAVAAQAVAAANAOQAMAOgBAUQABAWgMANQgNANgVAAQgVABgMgOgAgRgSQgFAHAAAMQAAALAFAIQAHAJAKAAQALAAAGgJQAGgIAAgLQgBgMgFgHQgGgJgLAAQgKAAgHAJg");
	this.shape_38.setTransform(-121.8,-162.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#013F5D").s().p("AAUAhIAAg9IgnAAIAAA9QAAAPgLAAQgLAAAAgPIAAhAQABgQAPAAIA0AAQAPAAgBAQIAABAQABAPgMAAQgKAAAAgPg");
	this.shape_39.setTransform(-131.7,-162.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#013F5D").s().p("ABHBSQgEgFgCgNIgPhfIgXBYQgDAOgFAGQgHAIgMAAQgLAAgHgIQgEgGgEgOIgWhYIgBAAIgPBfQgCANgEAFQgEAIgLAAQgSAAAAgTIACgOIATh2QACgMAIgIQAJgIAMAAQALAAAHAHQAHAGADALIAXBhIAAAAIAYhhQACgKAIgHQAIgHAKAAQAMAAAJAIQAIAIACAMIATB2IACAOQAAATgSAAQgLAAgEgIg");
	this.shape_40.setTransform(122.5,-208.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#013F5D").s().p("AAXA8IAAgrIgtA7QgKANgLgBQgIAAgGgEQgFgFAAgIQAAgJALgNIAggjQgTgEgMgMQgLgMAAgTQAAgYARgQQARgPAgAAIAfAAQAQABAFAFQAGAGAAARIAAB3QAAAcgUAAQgUAAAAgcgAgTgeQAAAWAYAAIASAAIAAgrIgTAAQgXAAAAAVg");
	this.shape_41.setTransform(104.8,-208.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#013F5D").s().p("AgkBZQgPAAgGgGQgGgHAAgPIAAh4QAAgdAUAAQAUAAAAAdIAAAeIAaAAQAdAAAQAPQAQAPAAAcQAAAdgSAQQgRAPgdAAgAgXA0IARAAQAaAAABgXQgBgXgaAAIgRAAg");
	this.shape_42.setTransform(90,-208.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#013F5D").s().p("AgpBVQgWgIAAgRQAAgPAQAAQAHAAAHADIANAFQAGABALAAQAaAAAAgUQAAgJgHgGQgEgFgKgBIgOgBQgZAAAAgPQAAgIAHgEQAFgEAJgBIAPAAQAIAAAGgEQAHgEAAgJQAAgSgXAAQgHAAgMADQgMADgGAAQgQAAAAgPQAAgFAEgFQAOgPAkAAQA+AAAAAxQAAAZgYAKIAAABQAcAKAAAdQAAA3hEAAQgWAAgPgFg");
	this.shape_43.setTransform(74.9,-208.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#013F5D").s().p("AggBXQABgGACgGIALgcIgyh5QgCgFAAgGQAAgJAFgFQAGgGAIAAQAMAAAGARIAiBVIAAAAIAghUQAEgJADgDQAEgGAJAAQASAAAAASQAAAIgCAGIhACjQgDAIgDADQgEAFgJAAQgRAAgBgTg");
	this.shape_44.setTransform(61.3,-207);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#013F5D").s().p("Ag/A8IAAh3QAAgRAGgGQAGgFAQgBIAjAAQAdAAARAPQASAQAAAdQAAAcgRAPQgQAQgcAAIgaAAIAAAdQAAAcgUAAQgUAAAAgcgAgXgFIARAAQAbAAgBgXQABgXgbAAIgRAAg");
	this.shape_45.setTransform(47.1,-208.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#013F5D").s().p("AA4BjQgFgEAAgJIAAgPIhlAAIAAAPQAAAJgFAEQgEAFgJAAQgIAAgFgFQgEgEAAgJIAAggQgBgRARAAQAEAAACgHQADgHAAgTIAAg/QAAgUAMgMQALgMAUAAIA8AAQANAAAFAIQAFAGAAAOIAABwQAUAAABARIAAAgQgBASgRAAQgJAAgEgFgAgYg7IAAA/QAAASgGAPIA3AAIAAhpIgoAAQgJAAAAAJg");
	this.shape_46.setTransform(30.1,-207);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#013F5D").s().p("AgjBZQgQAAgGgGQgGgHAAgPIAAh4QAAgdAUAAQAUAAAAAdIAAAeIAaAAQAdAAAQAPQAQAPAAAcQAAAdgSAQQgRAPgdAAgAgXA0IARAAQAaAAAAgXQAAgXgaAAIgRAAg");
	this.shape_47.setTransform(6.1,-208.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#013F5D").s().p("AgTA8IAAhuQgWAAgFgCQgQgDAAgOQAAgSAdgBIBEAAQAcABAAASQAAAOgQADQgFACgWAAIAABuQAAAcgTAAQgUAAAAgcg");
	this.shape_48.setTransform(-7.9,-208.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#013F5D").s().p("AAlBHIgHgVIg9AAIgHAWQgGASgNAAQgJAAgFgFQgFgFAAgJQAAgGACgFIAth7QAKgaAUAAQAVAAAIAXIAsB9QADAIAAAFQAAAIgFAFQgGAFgIAAQgPAAgGgTgAgTAPIAmAAIgTg8g");
	this.shape_49.setTransform(-21.6,-208.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#013F5D").s().p("AgpBVQgWgIAAgRQAAgPAQAAQAHAAAHADIANAFQAGABALAAQAaAAAAgUQAAgJgHgGQgEgFgKgBIgOgBQgZAAAAgPQAAgIAHgEQAFgEAJgBIAPAAQAIAAAGgEQAHgEAAgJQAAgSgXAAQgHAAgMADQgMADgGAAQgQAAAAgPQAAgFAEgFQAOgPAkAAQA+AAAAAxQAAAZgYAKIAAABQAcAKAAAdQAAA3hEAAQgWAAgPgFg");
	this.shape_50.setTransform(-36.4,-208.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#013F5D").s().p("AAlBHIgHgVIg9AAIgHAWQgGASgNAAQgJAAgFgFQgFgFAAgJQAAgGACgFIAth7QAKgaAUAAQAVAAAIAXIAsB9QADAIAAAFQAAAIgFAFQgGAFgIAAQgPAAgGgTgAgTAPIAmAAIgTg8g");
	this.shape_51.setTransform(-50.9,-208.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#013F5D").s().p("AAeBMIg0g9IgHAFIAAAqQAAAcgUAAQgUAAAAgcIAAh6QAAgdAUAAQAUAAAAAdIAAAmIAyg0QAOgPAHAAQAIAAAHAGQAFAFAAAIQAAAGgFAGIgKAKIgpAoIA1A8QALALAAAIQAAAIgGAFQgFAGgIAAQgKAAgLgOg");
	this.shape_52.setTransform(-65.3,-208.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#013F5D").s().p("AgvBCQgWgZAAgpQAAgmAXgaQAYgbAmAAQAOAAAMAEQAQAFAHAJQADAFAAAFQAAAHgEAFQgGAGgHAAQgGAAgKgEQgIgEgLAAQgVAAgLASQgKAPAAAVQAAAXALAOQAMAQAUAAQAKAAAMgFQALgEAFAAQAGAAAFAFQAFAGgBAHQAAAFgDAGQgNASgnAAQgnAAgXgag");
	this.shape_53.setTransform(-81.4,-208.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#013F5D").s().p("AguBCQgYgZAAgpQABgmAXgaQAYgbAmAAQAOAAANAEQAPAFAGAJQAFAFAAAFQgBAHgFAFQgFAGgGAAQgHAAgKgEQgJgEgKAAQgUAAgMASQgKAPAAAVQAAAXAKAOQAMAQAVAAQAKAAALgFQANgEADAAQAHAAAFAFQAEAGABAHQAAAFgFAGQgNASglAAQgoAAgWgag");
	this.shape_54.setTransform(-96.7,-208.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#013F5D").s().p("AAlBHIgHgVIg9AAIgHAWQgGASgNAAQgJAAgFgFQgFgFAAgJQAAgGACgFIAth7QAKgaAUAAQAVAAAIAXIAsB9QADAIAAAFQAAAIgFAFQgGAFgIAAQgPAAgGgTgAgTAPIAmAAIgTg8g");
	this.shape_55.setTransform(-112.1,-208.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#013F5D").s().p("Ag/A8IAAh3QAAgRAGgGQAGgFAQgBIAjAAQAdAAARAPQASAQAAAdQAAAcgQAPQgRAQgcAAIgaAAIAAAdQAAAcgUAAQgUAAAAgcgAgXgFIARAAQAaAAABgXQgBgXgaAAIgRAAg");
	this.shape_56.setTransform(-126.8,-208.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.instance = new lib.Symbol26();
	this.instance.parent = this;
	this.instance.setTransform(-229,-279);
	this.instance.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,4,15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol24, new cjs.Rectangle(-245,-291,496,591), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.fon = new lib.Symbol17();
	this.fon.parent = this;
	this.fon.setTransform(25,25,1,1,0,0,0,25,25);

	this.timeline.addTween(cjs.Tween.get(this.fon).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(0,0,50,50), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.hit = new lib.Symbol19();
	this.hit.parent = this;
	this.hit.setTransform(-19.8,-9.5);

	this.timeline.addTween(cjs.Tween.get(this.hit).wait(1));

	// Layer 2
	this.f1 = new lib.Symbol9();
	this.f1.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.f1).wait(1));

	// Layer 4
	this.f2 = new lib.Symbol18();
	this.f2.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.f2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-29.2,-31,42.1,55), null);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.fon = new lib.Symbol5();
	this.fon.parent = this;
	this.fon.setTransform(230,0);

	this.timeline.addTween(cjs.Tween.get(this.fon).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5copy, new cjs.Rectangle(-129,-642,1805,942), null);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.hit = new lib.Symbol15copy();
	this.hit.parent = this;
	this.hit.setTransform(-110.3,-21.1,1.589,0.562);

	this.timeline.addTween(cjs.Tween.get(this.hit).wait(1));

	// Layer 2
	this.f1 = new lib.Symbol3copy();
	this.f1.parent = this;
	this.f1.setTransform(19,19);

	this.timeline.addTween(cjs.Tween.get(this.f1).wait(1));

	// Layer 1
	this.f2 = new lib.Symbol2copy();
	this.f2.parent = this;
	this.f2.setTransform(19,18.9);

	this.timeline.addTween(cjs.Tween.get(this.f2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-116.3,-21.1,129.7,37.1), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.hit = new lib.Symbol15();
	this.hit.parent = this;
	this.hit.setTransform(-14.1,-26.1,2.39,0.645,0,0,0,-0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.hit).wait(1));

	// Layer 2
	this.f1 = new lib.Symbol3();
	this.f1.parent = this;
	this.f1.setTransform(-27,19);

	this.timeline.addTween(cjs.Tween.get(this.f1).wait(1));

	// Layer 1
	this.f2 = new lib.Symbol2();
	this.f2.parent = this;
	this.f2.setTransform(-26.8,19);

	this.timeline.addTween(cjs.Tween.get(this.f2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-14,-26.1,130.6,42.6), null);


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.txt = new lib.Symbol4();
	this.txt.parent = this;
	this.txt.setTransform(274.8,45.4);

	this.instance = new lib.logof2();
	this.instance.parent = this;
	this.instance.setTransform(41,344,0.5,0.5);

	this.instance_1 = new lib.i0009();
	this.instance_1.parent = this;
	this.instance_1.setTransform(71,0);

	this.instance_2 = new lib.txt2x2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(243,376,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance},{t:this.txt}]}).wait(1));

	// Layer 1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Egj7AXcMAAAgu3MBH3AAAMAAAAu3g");
	mask.setTransform(270,217);

	// Layer 4
	this.pic = new lib.Symbol5copy();
	this.pic.parent = this;
	this.pic.setTransform(40,67);

	var maskedShapeInstanceList = [this.pic];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.pic).wait(1));

	// Layer 3
	this.instance_3 = new lib.i0005();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol25, new cjs.Rectangle(0,0,538,436), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.b2 = new lib.Symbol8();
	this.b2.parent = this;
	this.b2.setTransform(468,57,1,1,0,0,180);

	this.b1 = new lib.Symbol8();
	this.b1.parent = this;
	this.b1.setTransform(19,57);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.b1},{t:this.b2}]}).wait(1));

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EghFAHxIAAviMBCLAAAIAAPig");
	mask.setTransform(243.8,57.4);

	// Layer 1
	this.cont = new lib.Symbol16();
	this.cont.parent = this;
	this.cont.setTransform(32,14);

	var maskedShapeInstanceList = [this.cont];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.cont).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-10.3,14,507.6,67), null);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var r = this;
			r.visible=false;
		
		setTimeout(zeroInit, 50);
		
		function zeroInit ()
		{
			inter.initialization(r);
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 3
	this.share = new lib.Symbol24();
	this.share.parent = this;
	this.share.setTransform(265,328.5);

	this.timeline.addTween(cjs.Tween.get(this.share).wait(1));

	// Layer 15
	this.dragZone = new lib.Symbol23();
	this.dragZone.parent = this;
	this.dragZone.setTransform(17.7,89.2);

	this.timeline.addTween(cjs.Tween.get(this.dragZone).wait(1));

	// photo
	this.photo = new lib.Symbol25();
	this.photo.parent = this;
	this.photo.setTransform(-4,40.2);

	this.timeline.addTween(cjs.Tween.get(this.photo).wait(1));

	// Layer 10
	this.bottom = new lib.Symbol14();
	this.bottom.parent = this;
	this.bottom.setTransform(22.1,450.2);

	this.timeline.addTween(cjs.Tween.get(this.bottom).wait(1));

	// hz
	this.hz = new lib.Symbol21();
	this.hz.parent = this;
	this.hz.setTransform(263.5,228.8);

	this.timeline.addTween(cjs.Tween.get(this.hz).wait(1));

	// fonspic
	this.instance = new lib.i00012();
	this.instance.parent = this;
	this.instance.setTransform(-4,439);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// btns
	this.shareBtn = new lib.Symbol7();
	this.shareBtn.parent = this;
	this.shareBtn.setTransform(381,606.2);

	this.saveBtn = new lib.Symbol6();
	this.saveBtn.parent = this;
	this.saveBtn.setTransform(123.5,606.2);

	this.b2 = new lib.Symbol1copy2();
	this.b2.parent = this;
	this.b2.setTransform(496.2,28.3);

	this.b1 = new lib.Symbol1();
	this.b1.parent = this;
	this.b1.setTransform(36.9,33.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.b1},{t:this.b2},{t:this.saveBtn},{t:this.shareBtn}]}).wait(1));

	// hitAr
	this.hitAr = new lib.Symbol22();
	this.hitAr.parent = this;
	this.hitAr.setTransform(14,38.2);

	this.timeline.addTween(cjs.Tween.get(this.hitAr).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(172,-214.8,1805,1172.9);
// library properties:
lib.properties = {
	width: 530,
	height: 640,
	fps: 24,
	color: "#8ACCD9",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/i00012.png", id:"i00012"},
		{src:"images/i00015.png", id:"i00015"},
		{src:"images/i00017.png", id:"i00017"},
		{src:"images/i00020.png", id:"i00020"},
		{src:"images/i00022.png", id:"i00022"},
		{src:"images/i00023.png", id:"i00023"},
		{src:"images/i00024.png", id:"i00024"},
		{src:"images/i00025.png", id:"i00025"},
		{src:"images/i00026.png", id:"i00026"},
		{src:"images/i00027.png", id:"i00027"},
		{src:"images/i00028.png", id:"i00028"},
		{src:"images/i00029.png", id:"i00029"},
		{src:"images/i00030.png", id:"i00030"},
		{src:"images/i00031.png", id:"i00031"},
		{src:"images/i00032.png", id:"i00032"},
		{src:"images/i00033.png", id:"i00033"},
		{src:"images/i00034.png", id:"i00034"},
		{src:"images/i00035.png", id:"i00035"},
		{src:"images/i00036.png", id:"i00036"},
		{src:"images/i00037.png", id:"i00037"},
		{src:"images/i00038.png", id:"i00038"},
		{src:"images/i00039.png", id:"i00039"},
		{src:"images/i00040.png", id:"i00040"},
		{src:"images/i00041.png", id:"i00041"},
		{src:"images/i00042.png", id:"i00042"},
		{src:"images/i00043.png", id:"i00043"},
		{src:"images/i00044.png", id:"i00044"},
		{src:"images/i00045.png", id:"i00045"},
		{src:"images/i00046.png", id:"i00046"},
		{src:"images/i00047.png", id:"i00047"},
		{src:"images/i00048.png", id:"i00048"},
		{src:"images/i00049.png", id:"i00049"},
		{src:"images/i0005.png", id:"i0005"},
		{src:"images/i00050.png", id:"i00050"},
		{src:"images/i00051.png", id:"i00051"},
		{src:"images/i00052pngcopy.png", id:"i00052pngcopy"},
		{src:"images/i00052pngcopy2.png", id:"i00052pngcopy2"},
		{src:"images/i00053pngcopy.png", id:"i00053pngcopy"},
		{src:"images/i00053pngcopy2.png", id:"i00053pngcopy2"},
		{src:"images/i00054.png", id:"i00054"},
		{src:"images/i00055.png", id:"i00055"},
		{src:"images/i00056.png", id:"i00056"},
		{src:"images/i00058.png", id:"i00058"},
		{src:"images/i00059.png", id:"i00059"},
		{src:"images/i0009.png", id:"i0009"},
		{src:"images/logof2.png", id:"logof2"},
		{src:"images/txt2x2.png", id:"txt2x2"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;