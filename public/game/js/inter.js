var inter = {
    root: {},
    prevPicture: "",
    setPicture: "",
    mobile: false,
    shareLink: "",
    //xmlrLink: "https://test.fruto-promo.ru/backend/api/shareImage",
    xmlrLink: "https://fruto-promo.ru/backend/api/shareImage",

    initialization: function (root) {

        var er = inter.root = root;

        initLoadingPics();
        initbtns();
        initBtms();
        initShare();

        er.hz.visible = false;
        er.dragZone.visible = false;
        er.share.visible = false;

        er.visible = true;
        su();

        //Share//////////////////////////////////////////

        function initShare() {
            engine.createScaleBtn(er.share.exit, exitF);

            engine.createScaleBtn(er.share.vk, vkF);
            engine.createScaleBtn(er.share.ok, okF);
            engine.createScaleBtn(er.share.fb, fbF);

            er.share.imgHolder.fon.visible = false;
            er.share.dy = Math.round(er.share.y)

            function exitF() {
                er.share.visible = false;
                blockBtns(false);
            }

            function postShare(social, url) {
                var message = {
                    type: 'share',
                    social: social,
                    url: url
                }
                var json = JSON.stringify(message);
                window.parent.postMessage(json, '*');
            }

            function vkF() {

                er.share.vk.mouseEnabled = false;
                er.share.vk.alpha = .5;

                //xmlr       

                //er.photo.cache(0, 0, 538, 436);
                er.photo.cache(-212, 0, 961, 436);
                var j = JSON.stringify({ service: "vk", file_contents: er.photo.getCacheDataURL() })
                er.photo.uncache();

                //console.log(j);

                var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                xmlhttp.open("POST", inter.xmlrLink);
                xmlhttp.setRequestHeader("Content-Type", "application/json");
                xmlhttp.send(j);

                xmlhttp.onload = function () {

                    var obj = JSON.parse(xmlhttp.responseText);
                    //    console.log(obj);
                    // vkontakte(obj.url);
                    postShare("vk", obj.url);

                    er.share.vk.mouseEnabled = true;
                    er.share.vk.alpha = 1;
                    su();
                }
            }

            function okF() {

                //xmlr       

                er.share.ok.mouseEnabled = false;
                er.share.ok.alpha = .5;

                //er.photo.cache(0, 0, 538, 436);
                er.photo.cache(-212, 0, 961, 436);
                var j = JSON.stringify({ service: "ok", file_contents: er.photo.getCacheDataURL() })
                er.photo.uncache();

                //console.log(j);

                var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                xmlhttp.open("POST", inter.xmlrLink);
                xmlhttp.setRequestHeader("Content-Type", "application/json");
                xmlhttp.send(j);

                xmlhttp.onload = function () {

                    var obj = JSON.parse(xmlhttp.responseText);
                    //   console.log(obj);
                    //odnoklassniki(obj.url);
                    postShare("ok", obj.url);

                    er.share.ok.mouseEnabled = true;
                    er.share.ok.alpha = 1;
                    su();
                }



                //odnoklassniki(inter.shareLink);
            }

            function fbF() {

                er.share.fb.mouseEnabled = false;
                er.share.fb.alpha = .5;

                //er.photo.cache(0, 0, 538, 436);
                er.photo.cache(-212, 0, 961, 436);
                var j = JSON.stringify({ service: "fb", file_contents: er.photo.getCacheDataURL() })
                er.photo.uncache();

                //console.log(j);

                var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                xmlhttp.open("POST", inter.xmlrLink);
                xmlhttp.setRequestHeader("Content-Type", "application/json");
                xmlhttp.send(j);

                xmlhttp.onload = function () {

                    var obj = JSON.parse(xmlhttp.responseText);
                    // console.log(obj);
                    //facebook2(obj.url);
                    postShare("fb", obj.url);

                    er.share.fb.mouseEnabled = true;
                    er.share.fb.alpha = 1;
                    su();
                }


                //facebook2(inter.shareLink);
            }
        }

        function sendShare() {



            //whenDoneCallBack
            showShare();
        }

        function showShare() {

            blockBtns(true);

            engineTouch.allFramesHide();
            er.photo.cache(0, 0, 538, 436);

            var pic = new createjs.Bitmap(er.photo.getCacheDataURL());

            er.photo.uncache();

            if (er.share.pic != undefined) {
                er.share.imgHolder.removeChild(er.share.pic);
            }

            er.share.pic = pic;
            pic.scaleX = pic.scaleY = .7;
            er.share.imgHolder.addChild(pic);

            er.share.visible = true;

            er.share.y -= 50;

            createjs.Tween.get(er.share).to({ y: er.share.dy }, 150).addEventListener("change", su);
        }

        function blockBtns(par) {
            er.saveBtn.mouseEnabled = !par;
            er.shareBtn.mouseEnabled = !par;

            er.b1.mouseEnabled = !par;
            er.b2.mouseEnabled = !par;

            er.bottom.mouseEnabled = !par;
        }



        ///////////////////////////////////////////////

        //loadPcis/////////////////////////////////////////
        function initLoadingPics() {
            let timerId = setInterval(checkPic, 500);


            function checkPic() {
                if (inter.prevPicture != inter.setPicture) {
                    inter.prevPicture = inter.setPicture;
                    //https://stackoverflow.com/questions/22710627/tainted-canvases-may-not-be-exported#:~:text=img.crossOrigin%3D%22anonymous%22

                    //  var img = document.createElement("img");
                    //  img.crossOrigin = 'Anonymous';
                    //  img.src = inter.setPicture;

                    // var img = 'data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';
                    var img = inter.setPicture;
                    var bitmap = new createjs.Bitmap(img);

                    if (er.photo.pic.bmc != undefined) {
                       // console.log(er.photo.pic.bmc);
                        if (!er.photo.pic.bmc.parent.isDelete) {
                            er.photo.pic.bmc.parent.deleteMc();
                        }
                    }

                    bitmap.alpha = 0;

                    bitmap.image.onload = handleObjLoad;
                    er.photo.pic.addChild(bitmap);
                    er.photo.pic.btm = bitmap;

                    function handleObjLoad() {
                        scalePic(bitmap);

                        var mc = new createjs.MovieClip();

                        var w = engine.mcWidth(bitmap);
                        var h = engine.mcHeight(bitmap);

                        var ws = w * bitmap.scaleX;
                        var hs = h * bitmap.scaleY;

                        mc.x = (460 - ws) / 2 + ws / 2;
                        mc.y = 0 + hs / 2;

                        /*
                        var hit = new createjs.Shape();
                        hit.graphics.beginFill("#CFA8FB").drawRect(-Math.round(ws / 2), -Math.round(hs / 2), ws, hs);
                        hit.alpha = .5;
                        mc.addChild(hit);
                        */

                        er.photo.pic.addChild(mc);
                        er.photo.pic.bmc = mc;
                        mc.addChild(bitmap);

                        bitmap.x = -ws / 2;
                        bitmap.y = -hs / 2;

                        var s = engineTouch.createSetup();
                        s.frame = true;
                        s.cacheScale = -1;
                        s.rotate = true;
                        s.scale = true;
                        s.frameBtnsSize = 16;
                        if (inter.mobile) {
                            s.frameBtnsSize = 40;
                        }

                        engineTouch.addTouch(mc, s);



                        su();
                    }
                }
            }



            function scalePic(img) {
                var w = engine.mcWidth(img);
                var h = engine.mcHeight(img);

                var bx, by;

                if (w <= h) {
                    img.scaleX = img.scaleY = 300 / h;
                }
                else {
                    img.scaleX = img.scaleY = 460 / w;

                    if (img.scaleY * h > 300) {
                        img.scaleX = img.scaleY = 300 / h;
                    }
                }

                bx = 230 - Math.ceil((w * img.scaleX) / 2);
                by = 150 - Math.ceil((h * img.scaleY) / 2);

                img.x = bx;
                img.y = by;

                createjs.Tween.get(img).to({ alpha: 1 }, 300).addEventListener("change", su);

                er.uncache();
                er.alpha = 1;
                var preloaderDiv = document.getElementById("_preload_div_");
                preloaderDiv.style.display = 'none';

                su();
            }
        }

        //init_fon_btns//////////////////////////////////////
        function initbtns() {
            er.b1.f2.visible = false;
            er.b2.f2.visible = false;
            engine.createMenuBtn(er.b1, nxtFon);
            engine.createMenuBtn(er.b2, prevFon);

            engine.createScaleBtn(er.saveBtn, savef);
            engine.createScaleBtn(er.shareBtn, sendShare);
        }

        function savef() {
            var link = document.createElement('a');
            link.target = "_blank"
            link.download = 'fruto-promo.png';
            engineTouch.allFramesHide();
            er.photo.cache(0, 0, 538, 436);            
            link.href = er.photo.getCacheDataURL();
            link.click();
            er.photo.uncache();
        }


        function nxtFon() {
            var fr = er.photo.pic.fon.currentFrame;
            fr++;
            if (fr > 3) {
                fr = 0;
            }
            er.photo.pic.fon.gotoAndStop(fr);
            er.photo.txt.gotoAndStop(fr);
        }

        function prevFon() {
            var fr = er.photo.pic.fon.currentFrame;
            fr--;
            if (fr < 0) {
                fr = 3;
            }
            er.photo.pic.fon.gotoAndStop(fr);
            er.photo.txt.gotoAndStop(fr);
        }
        ////////////////////////////////////////////////////

        //init_btns/////////////////////////////////////////
        function initBtms() {
            var cont = er.bottom.cont;
            var pic;
            var btn;
            var frame;

            for (let i = 0; i < 23; i++) {

                btn = new createjs.MovieClip();
                btn.numx = i;
                frame = new lib.frame();
                pic = new lib.pics();

                btn.frame = frame;

                btn.addChild(frame)
                btn.addChild(pic);
                cont.addChild(btn);
                pic.scaleX = pic.scaleY = 0.34;
                pic.gotoAndStop(i);

                pic.x = frame.x = 50;
                pic.y = frame.y = 43;

                btn.y = 0;
                btn.x = i * (100 + 7);

                frame.visible = false;

                btn.cache(0, 0, 100, 86, 2);

                createBottomBtns(btn);
            }

            engineTouch.stageInitDeselect(er.hitAr);
            engineTouch.enableDeleteBtn();

            cont.fon.visible = false;

            //
            er.bottom.b1.f2.visible = false;
            er.bottom.b2.f2.visible = false;
            cont.anim = false;
            engine.createMenuBtn(er.bottom.b1, scrollBtnsL);
            engine.createMenuBtn(er.bottom.b2, scrollBtnsR);

            analization();
        }

        function createBottomBtns(mc) {
            var hit = new createjs.Shape();
            hit.graphics.beginFill("#FF0000").drawRect(0, 0, 100, 86);
            mc.on("mouseover", mouseEVT);
            mc.on("mouseout", mouseEVT);
            mc.on("mousedown", mouseEVT);
            mc.on("pressmove", mouseEVT);
            mc.on("pressup", mouseEVT);

            hit.alpha = .5;
            // mc.addChild(hit);
            mc.hitArea = hit;

            function mouseEVT(evt) {
                evt.nativeEvent.preventDefault();

                if (evt.type == "mouseover") {

                    mc.frame.visible = true;
                    mc.updateCache();

                }
                else if (evt.type == "mouseout") {
                    mc.frame.visible = false;
                    mc.updateCache();

                }
                else if (evt.type == "mousedown") {
                    headMouseDownHandler(evt.currentTarget, evt, evt.stageX, evt.stageY);
                }
                else if (evt.type == "pressmove") {
                    headMoveHandler(evt.currentTarget, evt, evt.stageX, evt.stageY);
                }
                else if (evt.type == "pressup") {
                    headUpHandler(evt.currentTarget, evt, evt.stageX, evt.stageY);
                }

                su();
            }
            mc.cursor = "pointer";
        }

        //HEAD///////////////////////////

        function headMouseDownHandler(movObj, evt, EstageX, EstageY) {


            movObj.movingChar = new createjs.MovieClip();
            var pic = new lib.pics();
            movObj.movingChar.addChild(pic);
            pic.scaleX = pic.scaleY = 0.34;
            pic.gotoAndStop(movObj.numx);
            er.addChild(movObj.movingChar);

            movObj.movingChar.cache(-50, -43, 100, 86, 2);

            var local = er.globalToLocal(EstageX, EstageY);

            movObj.movingChar.x = Math.round(local.x);
            movObj.movingChar.y = Math.round(local.y);


        }
        function headMoveHandler(movObj, evt, EstageX, EstageY) {

            var local = er.globalToLocal(EstageX, EstageY);

            movObj.movingChar.x = Math.round(local.x);
            movObj.movingChar.y = Math.round(local.y);

            setBoundaries(movObj.movingChar);

            if (engine.hitTestCenter(movObj.movingChar, er.hz)) {
                movObj.movingChar.alpha = 1;
            }
            else {
                movObj.movingChar.alpha = .5;
            }
        }
        function headUpHandler(movObj, evt, EstageX, EstageY) {


            if (movObj.movingChar.alpha == 1) {
                addDragMc(movObj.movingChar);

            }
            else {
                er.removeChild(movObj.movingChar);
            }
        }


        function setBoundaries(mc) {



            if (mc.x < er.dragZone.x + 50) {
                mc.x = er.dragZone.x + 50;
            }

            if (mc.y < er.dragZone.y + 43) {
                mc.y = er.dragZone.y + 43;
            }

            if (mc.x > er.dragZone.x + 490 - 50) {
                mc.x = er.dragZone.x + 490 - 50;
            }

            if (mc.y > er.dragZone.y + 465 - 43) {
                mc.y = er.dragZone.y + 465 - 43;
            }


            mc.x = Math.round(mc.x);
            mc.y = Math.round(mc.y);
        }

        function addDragMc(mc) {
            er.photo.pic.addChild(mc);

            var p = engine.localToLocal(mc.x, mc.y, er, er.photo.pic)

            mc.x = p.x;
            mc.y = p.y;

            var s = engineTouch.createSetup();
            s.frame = true;
            s.rotate = true;
            s.scale = true;
            s.frameBtnsSize = 16;

            if (inter.mobile) {
                s.frameBtnsSize = 40;
            }

            s.dragCallMM = function (e) {

                var mc = e.currentTarget.parent;
                mc.alpha = 1;

                if (mc.x < 0) {
                    mc.alpha = .5
                }

                if (mc.y < 0) {
                    mc.alpha = .5
                }

                if (mc.x > 450) {
                    mc.alpha = .5
                }

                if (mc.y > 300) {
                    mc.alpha = .5
                }
            }

            s.dragCallMU = function (e) {
                var mc = e.currentTarget.parent;
                if (mc.alpha < 1) {
                    mc.deleteMc();
                }
            }

            engineTouch.addTouch(mc, s);
        }



        ///////////////////////////////////////////

        function scrollBtnsR() {
            var cont = er.bottom.cont;

            if (cont.anim) {
                return;
            }
            var nx = cont.x - 107 * 4;

            if (nx < -2001) {
                nx = -2001;
            }

            cont.anim = true;
            createjs.Tween.get(cont).to({ x: nx }, 400).call(analization).addEventListener("change", su);
        }

        function scrollBtnsL() {
            var cont = er.bottom.cont;

            if (cont.anim) {
                return;
            }

            var nx = cont.x + 107 * 4;

            if (nx > 32) {
                nx = 32;
            }

            cont.anim = true;
            createjs.Tween.get(cont).to({ x: nx }, 400).call(analization).addEventListener("change", su);
        }

        function analization() {
            var cont = er.bottom.cont;
            var btm = er.bottom;

            btm.b1.mouseEnabled = true;
            btm.b2.mouseEnabled = true;
            btm.b1.visible = true;
            btm.b2.visible = true;

            if (cont.x >= 32) {
                cont.x = 32
                btm.b1.visible = false;
            }


            if (cont.x <= -2001) {
                cont.x = -2001;
                btm.b2.visible = false;
            }

            cont.anim = false;

        }











    }
}