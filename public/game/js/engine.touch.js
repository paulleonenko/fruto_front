var engineTouch = {
    onlyOneSelected: true,
    dragArr: [],
    frameColor: "#E5E3E3",
    circle: null,
    //animate2015
    //mc transformpoint - center
    addTouch: function (img, setup) {

        var mc = new createjs.MovieClip();

        mc.x = img.x;
        mc.y = img.y;
        img.x = 0;
        img.y = 0;

        img.parent.addChild(mc);
        mc.mcOrigin = img;
        mc.addChild(img);

        var w = mc.w = engine.mcWidth(img);
        var h = mc.h = engine.mcHeight(img);

        mc.nominalBounds = {
            width: w,
            height: h,
        }

        var maxwh = mc.maxwh = (w < h) ? h : w;
        mc.d = maxwh * Math.sqrt(2);

        if (setup.cacheScale != -1) {
            img.cache(-Math.round((w / img.scaleX) / 2), -Math.round((h / img.scaleY) / 2), w / img.scaleX, h / img.scaleY, setup.cacheScale);
        }


        var drag = new createjs.MovieClip();
        mc.drag = drag;

        var hit = new createjs.Shape();
        hit.graphics.beginFill("#FF0000").drawRect(-Math.round(w / 2), -Math.round(h / 2), w, h);
        hit.alpha = .5;
        drag.hitArea = hit;
        //drag.addChild(hit);
        mc.addChild(drag);

        mc.isDelete = false; 
        mc.deleteMc = function () {
            var index = engineTouch.dragArr.indexOf(mc);
            engineTouch.dragArr.splice(index, 1);
            mc.drag.removeAllEventListeners();
            mc.parent.removeChild(mc);
            mc.isDelete = true;            
        }

        //frame
        if (setup.frame) {
            var frame = new createjs.MovieClip();
            mc.frame = frame;
            mc.addChild(frame);

            addFrameBtns();

            mc.frame.shape = new createjs.Shape();

            mc.redrawFrame = function () {
                var shape = mc.frame.shape;
                shape.graphics.clear();
                shape.graphics.setStrokeDash([4, 4]);
                var fw = maxwh * img.scaleX;
                shape.graphics.setStrokeStyle(3).beginStroke(engineTouch.frameColor).rect(-Math.round(fw / 2), -Math.round(fw / 2), fw, fw);
                frame.addChild(shape);

                mc.frame.btn1.x = -Math.round(fw / 2) - setup.frameBtnsSize / 2;
                mc.frame.btn1.y = -Math.round(fw / 2) - setup.frameBtnsSize / 2;

                mc.frame.btn2.x = Math.round(fw / 2) - setup.frameBtnsSize / 2;
                mc.frame.btn2.y = -Math.round(fw / 2) - setup.frameBtnsSize / 2;

                mc.frame.btn3.x = -Math.round(fw / 2) - setup.frameBtnsSize / 2;
                mc.frame.btn3.y = Math.round(fw / 2) - setup.frameBtnsSize / 2;

                mc.frame.btn4.x = Math.round(fw / 2) - setup.frameBtnsSize / 2;
                mc.frame.btn4.y = Math.round(fw / 2) - setup.frameBtnsSize / 2;

                mc.frame.delete.x = -Math.round(fw / 2) + fw / 2;
                mc.frame.delete.y = -Math.round(fw / 2) - 10;
                mc.frame.delete.parent.addChild(mc.frame.delete);


                //drag.removeChild(hit);
                hit.graphics.clear();
                hit.graphics.beginFill("#FF0000").drawRect(-Math.round(w * img.scaleX / 2), -Math.round(h * img.scaleX / 2), w * img.scaleX, h * img.scaleX);
                hit.alpha = .5;
                drag.hitArea = hit;
                //drag.addChild(hit);

            }
            mc.redrawFrame();



            mc.hideFrame = function () {
                mc.frame.visible = false;
            }

            mc.showFrame = function () {
                mc.frame.visible = true;
            }

            mc.hideFrame();

        }

        ///////////////////

        engineTouch.dragArr.push(mc);

        if (setup.drag) {
            drag.on("mousedown", function (evt) { evt.nativeEvent.preventDefault(); tileMouseDownHandler(evt.currentTarget.parent, evt, evt.stageX, evt.stageY); su(); });
            drag.on("pressmove", function (evt) { evt.nativeEvent.preventDefault(); tileMoveHandler(evt.currentTarget.parent, evt, evt.stageX, evt.stageY); su(); });
            drag.on("pressup", function (evt) { evt.nativeEvent.preventDefault(); tileUpHandler(evt.currentTarget.parent, evt, evt.stageX, evt.stageY); su(); });
            drag.cursor = "pointer";
        }

        function tileMouseDownHandler(movObj, evt, EstageX, EstageY) {

            var r = mc.rotation;
            mc.rotation = 0;
            var s = mc.scaleX;
            mc.scaleX = mc.scaleY = 1;

            var pt = movObj.globalToLocal(EstageX, EstageY);
            movObj.parent.addChild(movObj);
            movObj.pt = pt;

            if (movObj.frame != undefined) {
                movObj.fV = movObj.frame.visible;
            }
            movObj.sX = movObj.x;
            movObj.sY = movObj.y;

            mc.rotation = r;
            mc.scaleX = mc.scaleY = s;

            if (engineTouch.onlyOneSelected) {
                engineTouch.allFramesHide();
            }

            if (movObj.showFrame != undefined) {
                movObj.showFrame();
            }

            if (setup.dragCallMD != null) {
                setup.dragCallMD(evt);
            }



        }
        function tileMoveHandler(movObj, evt, EstageX, EstageY) {

            var r = mc.rotation;
            mc.rotation = 0;

            var s = mc.scaleX;
            mc.scaleX = mc.scaleY = 1;


            var pt = movObj.parent.globalToLocal(EstageX, EstageY);

            movObj.x = Math.round(pt.x - movObj.pt.x);
            movObj.y = Math.round(pt.y - movObj.pt.y);


            mc.rotation = r;
            mc.scaleX = mc.scaleY = s;


            if (setup.boundaries != null) {
                setBoundaries();
            }


            if (setup.dragCallMM != null) {
                setup.dragCallMM(evt);
            }




        }



        function setBoundaries() {

            var b = setup.boundaries;


            if (mc.x < b.x + mc.w / 2) {
                mc.x = b.x + mc.w / 2;
            }

            if (mc.y < b.y + mc.h / 2) {
                mc.y = b.y + mc.h / 2;
            }

            if (mc.x > b.x + b.w - w + mc.w / 2) {
                mc.x = b.x + b.w - w + mc.w / 2;
            }

            if (mc.y > b.y + b.h - h + mc.h / 2) {
                mc.y = b.y + b.h - h + mc.h / 2;
            }

            mc.x = Math.round(mc.x);
            mc.y = Math.round(mc.y);
        }




        function tileUpHandler(movObj, evt, EstageX, EstageY) {

            if (movObj.fV && movObj.sX == movObj.x && movObj.sY == movObj.y) {
                movObj.hideFrame();
            }

            if (setup.dragCallMU != null) {
                setup.dragCallMU(evt);
            }

        }

        function addFrameBtns() {
            mc.frame.btn1 = createBtn();
            mc.frame.btn2 = createBtn();
            mc.frame.btn3 = createBtn();
            mc.frame.btn4 = createBtn();

            mc.frame.delete = new lib.deletemc();



            mc.frame.btnArr = [mc.frame.btn1, mc.frame.btn2, mc.frame.btn3, mc.frame.btn4];

            mc.frame.btn1.x = -Math.round(maxwh / 2) - setup.frameBtnsSize / 2;
            mc.frame.btn1.y = -Math.round(maxwh / 2) - setup.frameBtnsSize / 2;

            mc.frame.delete.x = -Math.round(maxwh / 2) - 15;
            mc.frame.delete.y = -Math.round(maxwh / 2) - 20;

            mc.frame.btn2.x = Math.round(maxwh / 2) - setup.frameBtnsSize / 2;
            mc.frame.btn2.y = -Math.round(maxwh / 2) - setup.frameBtnsSize / 2;

            mc.frame.btn3.x = -Math.round(maxwh / 2) - setup.frameBtnsSize / 2;
            mc.frame.btn3.y = Math.round(maxwh / 2) - setup.frameBtnsSize / 2;

            mc.frame.btn4.x = Math.round(maxwh / 2) - setup.frameBtnsSize / 2;
            mc.frame.btn4.y = Math.round(maxwh / 2) - setup.frameBtnsSize / 2;

            for (var i = 0; i < mc.frame.btnArr.length; i++) {
                mc.frame.btnArr[i].id = i + 1;
                mc.frame.addChild(mc.frame.btnArr[i]);


                mc.frame.btnArr[i].on("mouseover", function (evt) { evt.nativeEvent.preventDefault(); evt.currentTarget.alpha = 1; su(); });
                mc.frame.btnArr[i].on("mouseout", function (evt) { evt.nativeEvent.preventDefault(); evt.currentTarget.alpha = .8; su(); });

                mc.frame.btnArr[i].on("mousedown", function (evt) { evt.nativeEvent.preventDefault(); rbMouseDownHandler(evt.currentTarget, evt, evt.stageX, evt.stageY); su(); });
                mc.frame.btnArr[i].on("pressmove", function (evt) { evt.nativeEvent.preventDefault(); rbMoveHandler(evt.currentTarget, evt, evt.stageX, evt.stageY); su(); });
                mc.frame.btnArr[i].on("pressup", function (evt) { evt.nativeEvent.preventDefault(); rbUpHandler(evt.currentTarget, evt, evt.stageX, evt.stageY); su(); });
                mc.frame.btnArr[i].cursor = "pointer";
            }

            mc.frame.addChild(mc.frame.delete);

            engine.createScaleBtn(mc.frame.delete, mc.deleteMc);



            function createBtn() {

                var btn = new createjs.MovieClip();

                var shape = new createjs.Shape();
               

                shape.graphics.beginFill(engineTouch.frameColor).drawRoundRectComplex(0, 0, setup.frameBtnsSize, setup.frameBtnsSize, 2, 2, 2, 2);
                btn.alpha = .8;
                btn.addChild(shape);

                btn.shape = btn;

                return btn;

            }
        }



        function rbMouseDownHandler(movObj, evt, EstageX, EstageY) {
        }

        function rbMoveHandler(movObj, evt, EstageX, EstageY) {

            var mc = movObj.parent.parent;
            ///ROTATE
            if (setup.rotate) {

                var center = { x: mc.x, y: mc.y }
                var current = { x: EstageX, y: EstageY }

                current = mc.parent.globalToLocal(current.x, current.y);


                var angle = engineTouch.getAngle(center, current);

                switch (movObj.id) {
                    case 1:
                        angle += 45;
                        break;
                    case 2:
                        angle -= 45;
                        break;
                    case 3:
                        angle += 135;
                        break;
                    case 4:
                        angle -= 135;
                        break;

                    default:
                        break;
                }

                mc.rotation = angle;

            }

            /////SCALE
            if (setup.scale) {


                var center = { x: mc.x, y: mc.y }
                var newPosition = { x: EstageX, y: EstageY }

                newPosition = mc.parent.globalToLocal(newPosition.x, newPosition.y);


                var delen = engineTouch.getVectorLength(center, newPosition) / (mc.d / 2);

                if (delen < .3) {
                    delen = .3;
                }

                img.scaleX = img.scaleY = delen;

                mc.redrawFrame();


            }
        }
        function rbUpHandler(movObj, evt, EstageX, EstageY) {

            var t = mc;

        }

    },
    createSetup: function () {
        var s = {
            drag: true,
            dragCallMD: null,
            dragCallMM: null,
            dragCallMU: null,
            boundaries: null, // boundaries {x:0, y:0, w: 100, h:100}
            frame: false,
            frameBtnsSize: 40,
            rotate: false,
            scale: false,
            cacheScale: 2,

        }

        return s;
    },
    allFramesHide: function () {
        for (var i = 0; i < engineTouch.dragArr.length; i++) {
            if (engineTouch.dragArr[i].hideFrame != undefined) {
                engineTouch.dragArr[i].hideFrame();
            }
        }
    },
    getAngle: function (p1, p2) {
        var currX = p1.x, currY = p1.y, endX = p2.x, endY = p2.y;
        var angle = Math.atan2(currX - endX, currY - endY) * (180 / Math.PI);

        if (angle < 0) {
            angle = Math.abs(angle);
        } else {
            angle = 360 - angle;
        }

        return angle;
    },
    getVectorLength: function (p1, p2) {
        var a = p1.x - p2.x;
        var b = p1.y - p2.y;

        var c = Math.sqrt(a * a + b * b);

        return c;
    },
    stageInitDeselect: function (mc) {

        mc.addEventListener("click", deselect);
        mc.alpha = 0.01;

        function deselect() {
            engineTouch.allFramesHide();
            su();
        }
    },
    enableDeleteBtn: function () {
        window.addEventListener('keyup', keyPressedup, true);

        function keyPressedup(event) {

            console.log(event.keyCode);

            switch (event.keyCode) {
                case 46:
                    for (let i = 0; i < engineTouch.dragArr.length; i++) {

                        if (engineTouch.dragArr[i].frame.visible) {

                            engineTouch.dragArr[i].deleteMc();
                            break;

                        }
                    }
                    su();
                    break;
            }
        }
    }
}
