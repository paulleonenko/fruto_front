var engine =
{
    createScaleBtn: function (mc, func, isLeft) {

        var hit = new createjs.Shape();

        if (isLeft == true) {
            hit.graphics.beginFill("#FF0000").drawRect(0, 0, this.mcWidth(mc), this.mcHeight(mc));
            mc.cache(-10, -10, this.mcWidth(mc) + 20, this.mcHeight(mc) + 20)
        }
        else {
            hit.graphics.beginFill("#FF0000").drawRect(-this.mcWidth(mc) / 2, -this.mcHeight(mc) / 2, this.mcWidth(mc), this.mcHeight(mc));
            mc.cache(-this.mcWidth(mc) / 2 - 10, -this.mcHeight(mc) / 2 - 10, this.mcWidth(mc) + 20, this.mcHeight(mc) + 20,2);
        }

        hit.alpha = .5;

        mc.on("mousedown", mouseEVT);
        mc.on("pressup", mouseEVT);

        //mc.addChild(hit);
        mc.hitArea = hit;

        //mc.over=false;

        function mouseEVT(evt) {
            evt.nativeEvent.preventDefault();
            if (evt.type == "mousedown") {
                createjs.Tween.get(mc, { override: true }).to({ scaleX: .95, scaleY: .95 }, 100, createjs.Ease.getPowIn(2.5)).addEventListener("change", su);
                //func(evt);
            }
            else if (evt.type == "pressup") {
                createjs.Tween.get(mc, { override: true }).to({ scaleX: 1, scaleY: 1 }, 100, createjs.Ease.getPowIn(2.5)).call(tweenComplete).addEventListener("change", su);;

                function tweenComplete() {
                    // if (mc.over)
                    // {
                    func(evt);
                    // }
                }
            }

            su();
        }
        mc.cursor = "pointer";


    },
    createMenuBtn: function (mc, func) {

        mc.on("mouseover", mouseEVT);
        mc.on("mouseout", mouseEVT);
        mc.on("mousedown", mouseEVT);
        mc.on("pressup", mouseEVT);

        //mc.addChild(hit);
        mc.hitArea = mc.hit;
        mc.hit.visible = false;

        //mc.over=false;

        function mouseEVT(evt) {
            evt.nativeEvent.preventDefault();

            // mc.parent.addChild(mc);

            if (evt.type == "mouseover") {
                // createjs.Tween.get(mc.pic, { override: true }).to({ scaleX: 1.05, scaleY: 1.05 }, 100, createjs.Ease.getPowIn(2.5)).addEventListener("change", su);;
                //mc.over=true;
                mc.f1.visible = false;
                mc.f2.visible = true;
                
            }
            else if (evt.type == "mouseout") {
                //  createjs.Tween.get(mc.pic, { override: true }).to({ scaleX: 1, scaleY: 1 }, 100, createjs.Ease.getPowIn(2.5)).addEventListener("change", su);;
                //mc.over=false;
                mc.f2.visible = false;
                mc.f1.visible = true;
               
            }
            else if (evt.type == "mousedown") {
                createjs.Tween.get(mc, { override: true }).to({ scaleX: .95, scaleY: .95 }, 100, createjs.Ease.getPowIn(2.5)).addEventListener("change", su);
                //func(evt);
            }
            else if (evt.type == "pressup") {
                createjs.Tween.get(mc, { override: true }).to({ scaleX: 1, scaleY: 1 }, 100, createjs.Ease.getPowIn(2.5)).call(tweenComplete).addEventListener("change", su);;

                function tweenComplete() {
                    // if (mc.over)
                    // {
                    func(evt);
                    // }
                }
            }

            su();
        }
        mc.cursor = "pointer";
    },
    createMenuBtn2: function (mc, func) {

        mc.hitArea = mc.hit;
        mc.hit.visible = false;



        mc.on("mouseover", mouseEVT);
        mc.on("mouseout", mouseEVT);
        mc.on("mousedown", mouseEVT);
        mc.on("pressup", mouseEVT);
       

        function mouseEVT(evt) {
            evt.nativeEvent.preventDefault();

            // mc.parent.addChild(mc);

            if (evt.type == "mouseover") {
                // createjs.Tween.get(mc.pic, { override: true }).to({ scaleX: 1.05, scaleY: 1.05 }, 100, createjs.Ease.getPowIn(2.5)).addEventListener("change", su);;
                //mc.over=true;
                mc.hand.visible = true;

            }
            else if (evt.type == "mouseout") {
                //  createjs.Tween.get(mc.pic, { override: true }).to({ scaleX: 1, scaleY: 1 }, 100, createjs.Ease.getPowIn(2.5)).addEventListener("change", su);;
                //mc.over=false;
                mc.hand.visible = false;
            }
            else if (evt.type == "mousedown") {
                createjs.Tween.get(mc, { override: true }).to({ scaleX: .95, scaleY: .95 }, 100, createjs.Ease.getPowIn(2.5)).addEventListener("change", su);
                //func(evt);
            }
            else if (evt.type == "pressup") {
                createjs.Tween.get(mc, { override: true }).to({ scaleX: 1, scaleY: 1 }, 100, createjs.Ease.getPowIn(2.5)).call(tweenComplete).addEventListener("change", su);;

                function tweenComplete() {
                    // if (mc.over)
                    // {
                    func(evt);
                    // }
                }
            }

            su();
        }
        mc.cursor = "pointer";
    },
    hitTest: function (obj1, obj2) {

        if (this.MacroCollision(obj1.x, obj1.y, this.mcWidth(obj1), this.mcHeight(obj1), obj2.x, obj2.y, this.mcWidth(obj2), this.mcHeight(obj2))) {
            return true;
        }
        return false;
    },
    hitTestCenter: function (obj1, obj2) {

        var w1 = this.mcWidth(obj1);
        var h1 = this.mcHeight(obj1);

        var w2 = this.mcWidth(obj2);
        var h2 = this.mcHeight(obj2);


        if (this.MacroCollision(obj1.x - w1 / 2, obj1.y - h1 / 2, w1, h1, obj2.x - w2 / 2, obj2.y - h2 / 2, w2, h2)) {
            return true;
        }
        return false;
    },
    /////hit test with Mouse
    hitTestX: function (obj1, mx, my, obj2) {

        //if (MacroCollision((obj1.x), (obj1.y), mcWidth(obj1), mcHeight(obj1), (obj2.x), (obj2.y), mcWidth(obj2), mcHeight(obj2)))
        if (this.MacroCollision(obj1.x, obj1.y, this.mcWidth(obj1), this.mcHeight(obj1), (mx - 25), (my - 25), 50, 50)) {
            //console.log("мышь"+ obj1.x+" "+ obj1.y+" "+this.mcWidth(obj1)+" "+this.mcHeight(obj1)+" "+(mx - 25)+" "+(my - 25)+" "+50+" "+ 50);
            return true;
        }
        else {
            if (this.MacroCollision(obj1.x, obj1.y, this.mcWidth(obj1), this.mcHeight(obj1), obj2.x, obj2.y, this.mcWidth(obj2), this.mcHeight(obj2))) {
                //console.log("объект"+this.mcWidth(obj1)+" "+this.mcHeight(obj1) +" "+this.mcWidth(obj2)+" "+ this.mcHeight(obj2));
                return true;
            }

            return false;
        }
    },

    hitTestXs: function (obj1, mx, my, obj2) {

        //if (MacroCollision((obj1.x), (obj1.y), mcWidth(obj1), mcHeight(obj1), (obj2.x), (obj2.y), mcWidth(obj2), mcHeight(obj2)))
        if (this.MacroCollision(obj1.x, obj1.y, this.mcWidth(obj1), this.mcHeight(obj1), (mx - 25), (my - 25), 50, 50)) {
            //console.log("мышь"+ obj1.x+" "+ obj1.y+" "+this.mcWidth(obj1)+" "+this.mcHeight(obj1)+" "+(mx - 25)+" "+(my - 25)+" "+50+" "+ 50);
            return true;
        }
        else {
            return false;
        }
    },

    MacroCollision: function (obj1x, obj1y, obj1w, obj1h, obj2x, obj2y, obj2w, obj2h) {
        var XColl = false;
        var YColl = false;

        if ((obj1x + obj1w >= obj2x) && (obj1x <= obj2x + obj2w)) XColl = true;
        if ((obj1y + obj1h >= obj2y) && (obj1y <= obj2y + obj2h)) YColl = true;

        if (XColl & YColl) {
            return true;
        }
        return false;
    },


    ////RANDOM////
    shuffleArray: function (array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    },

    checkArrayEquality: function (arr1, arr2) {

        if (arr1 == undefined || arr2 == undefined) {
            return false;
        }

        if (arr1.length != arr2.length) {
            return false;
        }
        else {
            for (var i = 0; i < arr1.length; i++) {
                if (arr1[i] != arr2[i]) {
                    return false;
                }
            }
        }
        return true;
    },
    mcWidth: function (mc) {

        var w = 1;

        try {
            if (mc.nominalBounds != undefined) {
                w = Math.floor(mc.scaleX * mc.nominalBounds.width);
            } else {
                w = Math.floor(mc.getBounds().width);
            }
        }
        catch (e) {
            console.log("mcWidth ERROR");
        }

        return w;
    },

    mcHeight: function (mc) {

        var h = 1;

        try {
            if (mc.nominalBounds != undefined) {
                h = Math.floor(mc.scaleY * mc.nominalBounds.height);
            } else {
                h = Math.floor(mc.getBounds().height);
            }
        }
        catch (e) {
            console.log("mcHeight ERROR");
        }

        return h;
    },

    setMcWidth: function (mc, newWidth) {
        mc.scaleX = newWidth / engine.mcWidth(mc);
    },
    setMcHeight: function (mc, newHeight) {
        mc.scaleY = newHeight / engine.mcHeight(mc);
    },


    ////////////////////////////////////////////////////////////////
    su: function () {
        stage.update();
    },

    getRandomInt: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    getRandom: function (min, max) {
        return min + (Math.random() * (max - min));
    },

    removeAllEventList: function (arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i].removeAllEventListeners();
        }
    },

    createVirtualObj: function () {
        return {
            x: 0,
            y: 0,
            scaleX: 1,
            scaleY: 1,
            nominalBounds: {
                width: 40,
                height: 40,
            }
        }
    },

    cache: function (mc, scale, isCenter) {
        if (isCenter === true) {
            mc.cache(-10 - (engine.mcWidth(mc) / 2), -10 - (engine.mcHeight(mc) / 2), engine.mcWidth(mc) + 20, engine.mcHeight(mc) + 20, scale);
        }
        else {
            mc.cache(-10, -10, engine.mcWidth(mc) + 20, engine.mcHeight(mc) + 20, scale);
        }

    },


    /////////////////////////////////////////////////
    getVectorLength: function (p1, p2) {
        var a = p2.x - p1.x;
        var b = p2.y - p1.y;

        var c = Math.sqrt(a * a + b * b);

        return c;
    },

    //кроссворд
    notCharKey: function (key) {
        switch (key) {
            case "Tab":
            case "Control":
            case "Backspace":
            case "Backspace":
            case "Delete":
            case "Escape":
            case "Shift":
            case "Enter":
            case "Insert":
            case "Alt":
            case "PageDown":
            case "PageUp":
            case "Meta":
            case "Meta":
            case "Meta":
            case "Meta":
            case "ArrowRight":
            case "ArrowLeft":
            case "ArrowDown":
            case "ArrowUp":
                return true;
            default: return false;
        }
    },
    localToLocal: function (x, y, from, to) {
        var g = from.localToGlobal(x, y);
        var l = to.globalToLocal(g.x, g.y);

        return l;
    },
    /////////////////////////////////
    blockCtrlZoom: function () {
        window.addEventListener("wheel", onWheel, { passive: false });
        window.addEventListener('keydown', keyPressed, true);
        //window.addEventListener('keyup', keyPressedup, true);

        function onWheel(e) {
            e = e || window.event;

            if (e.ctrlKey) {
                e.preventDefault();
            }
        }

        function keyPressed(e) {
            switch (e.keyCode) {
                case 107:
                case 109:
                    if (e.ctrlKey) {
                        e.preventDefault();
                    }
                    break;
                default:
                    break;
            }
        }
    }

}

function su() {
    stage.update();
}
engine.blockCtrlZoom();
